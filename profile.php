<?php include "header.php";?>

<div class="inctance_bg"></div>

<div class="parent">

	<div>
		<div class="header">
			<div class="wrap">
				<img src="<?php print$user["photo"] ?>" alt="">
				<div class="add_img">
					<form action="server.php" method="POST" enctype="multipart/form-data" class = "my_form">
						<label for="pic">Ընտրել</label>
						<input type="file" name="photo" class="prof_pic" id="pic" style="display: none">		
					</form>
				</div>

			</div>	
		</div>
		<div class="my">
			<div class="user_data">
				<p class="name"><?php print$user["name"].' '.$user["surname"]?></p>
			</div>
			<div class="actions">
				<ul class="my_list">
					<li><a class="btn btn-sx btn-danger"href="#">a</a></li>
					<li><a class="btn btn-sx btn-info"href="#">b</a></li>
					<!-- <li><a class="btn btn-sx btn-success"href="#">c</a></li>
						<li><a class="btn btn-sx btn-info"href="#">d</a></li> -->
				</ul>
			</div>			
		</div>

			<!-- /*center s*/ -->
	</div>
	<div class="center">

		<div class="menu">
			<div class="top">
				<div class="pub">
					<div class="img">

						<img src="<?=$user["photo"]?>" alt="">
					</div>
					<textarea name="" id="" cols="30" rows="10" placeholder="Ավելացրեք Գրառում . . . ."></textarea>
				</div>
				<div class="add_type">
					<ul class="types">
						<li>
							<form action="server.php" method="post" enctype="multipart/form-data" id="forma">
								<label for="upl" class="btn btn-success btn-sm"><img src="https://img.icons8.com/wired/25/000000/pictures-folder.png"></label>
								<input type="file" id="upl" name="post_img">	
								<input type="hidden" class="var" name="text">
							</form>						
						</li>

						<li><button class="btn btn-info share">Ավելացնել</button></li>
					</ul>
					<div class="info"></div>
				</div>
			</div>
			 

		</div>
	<div class="posts"></div>

	</div>
	<div class="right-part position-relative d-flex flex-column">
		<div class="events position d-flex mb-2">
			<div class="ev ev_show">
				<button class="bt_ev btn_add">Ավելացնել իրադաձություն</button>
			</div>
			<ul class="ev ev_show navbar-nav">
				<li class="av-item dropdown">
					<a class="nav-link dropdown-toggle bt_ev btn_show" href="#" id="navbardrop" data-toggle="dropdown">Իրադաձություններ</a>
				    <div class="dropdown-menu">
				        <a class="dropdown-item my_ev" href="#">Իմ</a>
				        <a class="dropdown-item fr_ev" href="#">Ընկերներ</a>
			      	</div>
				</li>				
			</ul>
		</div>	
		<div class="ev_contain"></div>
	
	</div>
</div>

<div class="hid_ev_reg" style="display: none">

<!-- event forma -->
	<div class=" evnt_form flex-column pr-2">
		<div class="exit_ev"><i class="far fa-times-circle text-danger exit_ev_ev"></i></div>
			<div class="form_ev mt-5 border-top">
				<form action="server.php" method="POST" class="fL_ev d-flex flex-column" enctype="multipart/form-data" name="event_form_send">
					<div class="dd1 d-flex mt-3">
						<div class="d-sz dd1-1">Իրադարձության նկար</div>
						<div class="dd1-2 d-flex">
							<div class="hd d-flex justify-content-center w-100">
								<label for="for" class="hd_r d-flex align-items-center p-2" style="cursor:pointer;display: block;">
									<div class="th_img mr-2">
										<input type="file" name="img_event" value="" id="for" style="display: none">
										<img src="https://img.icons8.com/color/20/000000/xlarge-icons.png" style="object-fit: cover">
									</div>
									<p class="m-0">Ընտրեք թեմայի նկարը</p>
								</label>
							</div>
						</div>
					</div>
					<div class="dd2 d-flex">
						<div class="d-sz dd2-1">Իրադարձության անվանումը</div>
						<div class="dd2-2 flex-fill form-label-group">
							<input type="text" name="event_name" class="event_name w-100 form-control shadow-none">
						</div>
					</div>
					<div class="dd3 d-flex justify-content-between">
						<div class="d-sz dd3-1">Գտնվելու վայրը</div>
						<div class="dd3-2 flex-fill form-label-group">
							<input type="text" name="event_place" class="event_place w-100 form-control shadow-none">
						</div>
					</div>
					<div class="dd4 d-flex">
						<div class="d-sz dd4-1">Նկարագրությունը</div>
						<div class="dd4-2 flex-fill form-label-group">
							<textarea name="event_descrip" class="event_descrip w-100 form-control shadow-none"></textarea>
						</div>
					</div>
					<div class="dd5 d-flex">
						<div class="d-sz dd5-1">Սկիզբ</div>
						<div class="dd5-2 d-flex form-label-group">
							<input type="date" class="event_date_start form-control shadow-none" name="event_date_start">
							<input type="time" class="event_time_start form-control shadow-none ml-3" name="event_time_start">
						</div>
					</div>
					<div class="dd6 d-flex">
						<div class="d-sz dd6-1">Ավարտ</div>
						<div class="dd5-2 d-flex form-label-group">
							<input type="date" class="event_date_end form-control shadow-none" name="event_date_end">
							<input type="time" class="event_time_end form-control shadow-none ml-3" name="event_time_end">
						</div>
					</div>
					<!-- <div class="dd7">
						<div class="dd7-1"></div>
						<div class="dd7 2"></div>
					</div> -->
					<div class="bt_wr d-flex justify-content-center">						
						<button name="event_form_send" class="mt-5 w-25">Ստեղծել</button>
					</div>
				</form>
			</div>
			<div class="alias_ev" style="display: none">
				
			</div>
		
	</div>
</div>
	<!--  for sharing data -->	
	<!-- dinamicaly created posts -->
	<script src="js/comment.js"></script>
	<script src="js/events.js"></script>
	<?php include "footer.php"; ?>
