<?php 
require_once "basemodel.php";
class Login extends BaseModel{
	function __construct() {
		session_start();
		parent:: __construct();
		if(isset($_POST["action"])){
			$action = $_POST["action"];
			call_user_func([$this, $action]);
		} 
		#login part
		if(isset($_POST["submit"])){
			$this->user_login();
		} 
		#add photo (page)
		if(isset($_FILES["photo"])){
			$this->photo_save();
		}
		if (isset($_POST["event_form_send"])){
			$this->event_form_send();
		}
		if(isset($_POST["text"]) || isset($_POST["post_img"])){
			$this->simple_post();
		}
		#change password
		if(isset($_POST["ch_passs"])) {
			// header("location: index.php");
		}
		// if(isset($_FILES["img_event"])){
  //   		$this->img_event();
  //   	}
		# add my Photos
		if(isset($_FILES["filee"])) {
			$this->add_my_photos();
		}
	}
	function validate() {
		#validate & insert
		$name      = htmlspecialchars($_POST["name"]);
		$surname   = htmlspecialchars($_POST["surname"]);
		$email     = htmlspecialchars($_POST["email"]);
		$age       = htmlspecialchars($_POST["age"]);
		$password  = htmlspecialchars($_POST["password"]);
		$conf_pass = htmlspecialchars($_POST["conf_pass"]);
		$error = [];
		$data = [];
		$check_email = $this->db->query("SELECT * FROM user WHERE email = '$email'")->fetch_all(true);
		if(empty($name)){
			$error["name"] = "Լրացրեք Ձեր անունը";
		} else $data["name"] = $name;
		if(empty($surname)){
			$error["surname"] = "Լրացրեք Ձեր ազգանունը"; 
		} else $data["surname"] = $surname;
		if(empty($email)){
			$error["email"] = "Լրացրեք Email դաշտը";
		}elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$error["email"] = "Սխալ Email ֆորմատ";
		}elseif(count($check_email) > 0){
			$error["email"] = "Ձեր Email հասցեից արդեն գոյություն ունի";
		} else $data["email"] = $email;
		if(empty($age)){
			$error["age"] = "Լրացրեք Ձեր տարիքը";
		}elseif (!filter_var($age, FILTER_VALIDATE_INT)) {
			$error["age"] = "Լրացրեք Ձեր տարիքը թվանշաններով";
		} else $data["age"] = $age;
		if(empty($password)){
			$error["password"] = "Լրացրեք գաղտնաբառը";
		} elseif (strlen((string)$password) < 6) {
			$error["password"] = "Գաղտնաբառը պետք է լինի 6 նիշից ավել";
		} else $data["password"] = $password;
		if(empty($conf_pass)){
			$error["confPass"] = "Լրացրեք գաղտնաբառը";
		} elseif (strcmp($password, $conf_pass) != 0) {
			$error["confPass"] = "Չի համապատասխանում գաղտանաբառին";
		}
		if(count($error) > 0){
			print json_encode($error);
		}  
		else {
			$data["password"] = password_hash($password, PASSWORD_DEFAULT);
			$this->insert("user", $data);
			print "true";	
		}
	}
	function user_login() {
		$login = $_POST["login"];
		$password = $_POST["password_fix"];
		$dataa = $this->db->query("SELECT * FROM user where email= '$login'")->fetch_all(true);
		$arr = [];
		if(empty($dataa)){
			$arr['error'] = "Chgarncvac email hase";
		} else {
			$user = $dataa[0];
			if (!password_verify($password, $user["password"])) {
				$arr['pass_error'] = "Sxal password";
				$_SESSION["email"] = $login;
			}
		}
		if (count($arr) > 0 ) {
			$_SESSION["logged_error"] = $arr;
			header("location:index.php");			
		}
		else{
			$_SESSION["user"] = $user;
			$id = $user["id"];
			$this->update("user", ["online"=>1], ["id"=>$id]);
			header("location:profile.php");
		}
	}
	#detect users becoming in offline 
	function add_to_ofline() {
		$id   = $_SESSION["user"]["id"];
		$this->update("user", ["online"=> 0], ["id"=> $id]);
	}
	#save my photo	
	function photo_save() {
		$id   = $_SESSION["user"]["id"];
		$temp = $_FILES["photo"]["tmp_name"];
		$name = $_FILES["photo"]["name"];
		$new_adress = "images/".time().$name;
		if(!file_exists("images")){
			mkdir("images");
		}
		move_uploaded_file($temp, $new_adress);
		$this->update("user", ["photo"=>$new_adress], ["id"=>$id]);
		$_SESSION["user"]["photo"] = $new_adress;
		header("location: profile.php");
	}
	// edit user data
	function edit_user_data() {
		$data = $_POST["changed_self_data"];
		$id = $_SESSION["user"]["id"];
		$name      = htmlspecialchars($_POST["changed_self_data"]["name"]);
		$surname   = htmlspecialchars($_POST["changed_self_data"]["surname"]);
		$age       = htmlspecialchars($_POST["changed_self_data"]["age"]);
		$email     = htmlspecialchars($_POST["changed_self_data"]["email"]);
		$check_email = $this->db->query("SELECT * FROM user WHERE email = '$email' AND id != '$id'")->fetch_all(true);
		$error = [];
		if(empty($name)){
			$error["name"] = "Լրացրեք Ձեր անունը";
		} 
		if(empty($surname)){
			$error["surname"] = "Լրացրեք Ձեր ազգանունը"; 
		}
		if(empty($email)){
			$error["email"] = "Լրացրեք Email դաշտը";
		}elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$error["email"] = "Սխալ Email ֆորմատ";
		}elseif(count($check_email) > 0){
			$error["email"] = "Ձեր Email հասցեից արդեն գոյություն ունի";
		}
		if(empty($age)){
			$error["age"] = "Լրացրեք Ձեր տարիքը";
		}elseif (!filter_var($age, FILTER_VALIDATE_INT)) {
			$error["age"] = "Լրացրեք Ձեր տարիքը թվանշաններով";
		} 
		
		if(count($error) > 0){
			print(json_encode($error));
		} else
		{
			$this->update("user", $data, ["id"=>$id]);
				$_SESSION["user"]["name"] = $data["name"];
				$_SESSION["user"]["surname"] = $data["surname"];
				$_SESSION["user"]["age"] = $data["age"];
				$_SESSION["user"]["email"] = $data["email"];	
				print "true";		
		}

	}
	// function change_password() {
	// 	header("location: index.php");
	// }
	function check_ch_pass(){
		$my_id = $_SESSION["user"]["id"];
		$old_pass = $_POST["old_pass"];
		$pass = $_POST["ch_pass"];
		$new_pass = password_hash($pass, PASSWORD_DEFAULT);
		$conf_pass = $_POST["conf_ch_pass"];
		$my_pass = $this->db->query("SELECT password FROM user WHERE id = '$my_id'")->fetch_all(true)[0];
		$ch_pass_error = [];
		if($pass != $conf_pass) {
			$ch_pass_error["c_pass"] = "Գաղտնաբառերը չեն համապատասխանում";
		} 
		if(strlen((string)$pass) < 6){
			$ch_pass_error["pass"] = "Նիշերի քանակը պետք է գերազանցի 6-ը";
		}
		if(empty($conf_pass) || empty($pass)){
			$ch_pass_error["empty"] = "Լրացրեք բոլոր դաշտերը";
		}
		if(!password_verify($old_pass, $my_pass["password"])){
			$ch_pass_error["old_pass"] = "Հին գաղտնաբառը սխալ է";
		}
		if(empty($ch_pass_error)) {
			$id = $_SESSION['user']['id'];
			$this->update("user", ['password'=>$new_pass], ["id"=>$id]);
			print "true";
		} else {	
			print_r(json_encode($ch_pass_error));
		}
	}
	#search
	function search() {
		$value = $_POST["value"];
		$my_id = $_SESSION["user"]["id"];
		$result = $this->find("user", $value);
		$data = [];

		foreach ($result as $key) {
			$user_id=$key['id'];
			$fr = $this->db->query("SELECT * FROM friend WHERE (my_id = $my_id AND user_id = $user_id) or (my_id = $user_id AND user_id = $my_id)")->fetch_all(true);
			$my_req = $this->db->query("SELECT * FROM request WHERE my_id = $my_id AND user_id = $user_id")->fetch_all(true);
			$user_req = $this->db->query("SELECT * FROM request WHERE my_id = $user_id AND user_id = $my_id")->fetch_all(true);
			if (!empty($fr)) {
				$key['status'] = "friend";

			}
			elseif(!empty($my_req)){
				$key['status'] = "my_request";

			}
			elseif (!empty($user_req)) {
				$key['status'] = "user_request";
				
			}
			elseif ($my_id==$user_id) {
				$key['status'] = "my_id";
				
			}
			else{
				$key['status'] = "add";

			}
			$data[]=$key;
		}
		print_r(json_encode($data));
		
	}
	#add request
	function add_request() {
		$user_id = $_POST["user_id"];
		$my_id = $_SESSION["user"]["id"];
		$this->insert("request", ["my_id"=>$my_id, "user_id"=>$user_id]);
		$sended_request = $this->db->query("SELECT user_id FROM request WHERE my_id = '$my_id'")->fetch_all(true);
		$_SESSION["sended_request"] = $sended_request;
		// print $sended_request;
	}
	#remove request
	function remove_request() {
		$user_id = $_POST["user_id"];
		$my_id = $_SESSION["user"]["id"];
		$this->delete("request", ["user_id"=>$user_id]);
		$sended_request = $this->db->query("SELECT user_id FROM request WHERE $my_id = '$my_id'")->fetch_all(true);
		$_SESSION["sended_request"] = $sended_request;

	}
	# get all request 
	function get_requests(){
		$my_id = $_SESSION["user"]["id"];
		$data = $this->db->query("SELECT user.* FROM user JOIN request
		  ON user.id = request.my_id WHERE user_id = '$my_id'")->fetch_all(true);
		print(json_encode($data));
				
	}
	#request amount
	// function get_requests_amount() {
	// 	$data = $this->db->query("SELECT user.* FROM user JOIN request
	// 					  ON user.id = request.my_id WHERE user_id = '$my_id'")->fetch_all(true);
	// 	print(json_encode($data));
	// }

	# friendship request remoove
	function remove_request_my() {
		$id = $_POST["user_id"];
		$my_id = $_SESSION["user"]["id"];
		$this->delete("request", ["my_id" => $id]);
		$sended_request = $this->db->query("SELECT user_id FROM request WHERE $my_id = '$my_id'")->fetch_all(true);
		$_SESSION["sended_request"] = $sended_request;
	}
	# friendship request accept
	function add_friend() {
		$id = $_POST["user_id"];
		$my_id = $_SESSION["user"]["id"];
		$this->insert("friend", ["my_id"=>$my_id, "user_id"=>$id]);
		$this->delete("request", ["user_id"=>$my_id]);
	}
	#get friends
	function get_friend(){
		$my_id = $_SESSION["user"]["id"];
		$data = $this->get_friend_inc($my_id);
		print(json_encode($data));
		//print $data;
	}
	#get friend friends(Mutual friends) 
	function get_friend_friends() {
		$my_id = $_SESSION["user"]["id"];
		$fr_id = $_POST["fr_id"];
		// $data = $this->db->query("SELECT id,name, surname, photo FROM user WHERE id IN(
		// 		SELECT DISTINCT my_id  
		// 		FROM friend 
		// 		WHERE 
		// 		user_id IN ($fr_id,$my_id) 
		// 		AND friend.my_id NOT IN ($fr_id,$my_id)
		// 		)")->fetch_all(true);
		$data2 = $this->db->query("SELECT user.id FROM user JOIN request 
					ON user.id = request.user_id
					WHERE my_id = 8 and user_id IN(
					SELECT user_id FROM friend WHERE my_id = '$fr_id'
					UNION 
					SELECT my_id FROM friend WHERE user_id = '$fr_id'
					)")->fetch_all(true);
		$d1 = [];
		$d2 = [];
		$data1   = $this->get_friend_inc($fr_id);
		$data1_1 = $this->get_common_friends($fr_id);
		$data1_2 = $this->get_common_friends($my_id);

		foreach ($data1_1 as $key => $value) {
			$d1[] = $value["id"];
		}
		foreach ($data1_2 as $key => $value) {
			$d2[] = $value["id"];
		}

		$mutual = array_intersect($d1, $d2);
		$value = implode("','", $mutual);
		$a = $this->db->query("SELECT id,name, surname, photo FROM user WHERE id IN('$value')")->fetch_all(true);
		print(json_encode([$data1, $a, $data2]));
	}
	# delete friend from FriendList
	function del_friend() {
		$my_id = $_SESSION["user"]["id"];
		$id = $_POST["friend_id"];
		$this->delete('friend', ["my_id"=>$id, "user_id"=>$my_id]);
		$this->delete('friend', ["my_id"=>$my_id, "user_id"=>$id]);
	}
	# send message (Add to database)
	function send_msg() {
		$my_id   = $_SESSION["user"]["id"];
		$id      = $_POST["user_id"];
		$message = mysqli_real_escape_string($this->db, $_POST["msg_val"]);
		$message = htmlspecialchars($message);
		// $time    = date("Y/m/d h:i:sa");
		$time = time();
		$this->insert("message", ["my_id"=>$my_id, "user_id"=>$id, "message"=>$message, "time"=>$time]);
	}
	#get 10 latest mesasages
	function get_messages() {
	$my_id  = $_SESSION["user"]["id"];
	$id = $_POST["user_id"];
	//$this->update("message", ["status"=>1], ["my_id"=>$my_id, "user_id"=>$id]);
	$send_msg = $this->db->query("SELECT user_id, message, `time` FROM message WHERE (my_id = '$my_id' AND user_id = '$id') OR (my_id = '$id' AND user_id = '$my_id') ORDER BY `time`")->fetch_all(true);
	// $snd_msg = [];
	// foreach($send_msg as $value){
	// 	$value["time"] = date("Y/m/d h:i:sa", $value["time"]);
	// }
	// print_r($send_msg);
	print_r(json_encode($send_msg));
	// $received_msg = $this->db->query("SELECT message FROM message WHERE my_id = '$id'");
	}
	#find friends
	function find_friends(){
		$value   = $_POST["value"];
		$my_id = $_SESSION['user']['id'];
		$srch  = $this->db->query("SELECT * FROM user WHERE id in
			(SELECT my_id from friend where user_id = $my_id
			UNION
			SELECT user_id from friend where my_id = $my_id)
			AND (name LIKE '%$value%' OR surname LIKE '%value%') ")->fetch_all(true);
			
		print(json_encode($srch));
		
	}
	#get all messages count
	function msg_count() {
		$my_id  = $_SESSION["user"]["id"];
		$data = $this->db->query("SELECT *,count(*) FROM message where open = 0 
			GROUP BY my_id having user_id = '$my_id'")->fetch_all(true);
		print count($data);
	}
	#get_messages
	function get_all_messages() {
		$my_id  = $_SESSION["user"]["id"];
		$result = $this->db->query("SELECT user.name,message.my_id, user.surname,user.photo, message.user_id, user.online,count(*) as  count FROM message 
					JOIN user
					ON user.id = message.my_id where status = 0 
					GROUP BY my_id having message.user_id ='$my_id'")->fetch_all(true);
		$this->update("message", ["open"=>1], ["user_id"=>$my_id]);
		print(json_encode($result));
			
	}
	# incoming Messages handling after click the button .incoming_msg
	function inc_msg() {
		$my_id  = $_SESSION["user"]["id"];
		$user_id = $_POST["user_id"];
		$this->update('message',["status"=>1], ["user_id"=> $my_id, "my_id"=>$user_id]);
	}

	#get my posts
	function get_posts() {
		$my_id  = $_SESSION["user"]["id"];
		$posts  = $this->db->query("SELECT post.*,user.name,user.surname,user.photo FROM 
			post JOIN user 
			ON user.id = post.my_id WHERE my_id = '$my_id' OR my_id in (SELECT my_id from friend where user_id = $my_id
			UNION
			SELECT user_id from friend where my_id = $my_id) ORDER BY post.time DESC")->fetch_all(true);

		$data = [];

		foreach ($posts as $v) {

			$post_id = $v['id'];

			$v['comments'] = $this->db->query("SELECT user.id, name,surname, photo, comment, time FROM user 
			JOIN comment ON user.id = comment.user_id	
			WHERE comment.post_id = '$post_id'")->fetch_all(true);

			$v['likes'] = $this->db->query("SELECT * FROM user 
			JOIN `like` ON user.id = `like`.user_id	
			WHERE `like`.post_id = '$post_id'")->fetch_all(true);
			$data[] = $v;
		}
		print(json_encode($data));
	}
	 #get friend(user) posts
    function get_fr_posts() {
    	$fr_id = $_POST["fr_id"];
    	$data = $this->db->query("SELECT * FROM post WHERE my_id = '$fr_id'")->fetch_all(true);
    	$res = [];
    	foreach($data as $val){
    		$post_id = $val['id'];
    		$val["likes"] =	$this->db->query("SELECT * FROM user 
				JOIN `like` ON user.id = `like`.user_id	
				WHERE `like`.post_id = '$post_id'")->fetch_all(true);

			$val['comments'] = $this->db->query("SELECT user.id, name,surname, photo, comment, time FROM
			 user JOIN comment ON user.id = comment.user_id	WHERE comment.post_id = '$post_id' ORDER BY
			 time DESC")->fetch_all(true);
			$res[] = $val; 
    	}

    	print json_encode($res);
    }
	#add simple post
	function simple_post() {
		var_dump($_FILES);
		$my_id = $_SESSION["user"]["id"];
		$post = $_POST["text"];
		$new_adress1 = "";
		$new_adress2 = "";
		if(!empty($_FILES["post_img"]["name"])){
			$temp = $_FILES["post_img"]["tmp_name"];
			$name = $_FILES["post_img"]["name"];
			if($_FILES["post_img"]["type"] == "video/mp4"){
				$new_adress2 = "images_post_vidst/".time().$name;
				move_uploaded_file($temp, $new_adress2);
			}else {
				$new_adress1 = "images_post/".time().$name;
				move_uploaded_file($temp, $new_adress1);
			}
			
			if(!file_exists("images_post")){
				mkdir("images_post");
			}
			if(!file_exists("images_post_vids")){
				mkdir("images_post_vidst");
			}			
		}

		$this->insert("post", ["my_id"=>$my_id, "post"=>$post, "post_img"=>$new_adress1, "post_video"=>$new_adress2]);
		
		header("location: profile.php");
	}	 
	
	# add_my_photos
	function add_my_photos() {
		$id   = $_SESSION["user"]["id"];
		$temp = $_FILES["filee"]["tmp_name"];
		$name = $_FILES["filee"]["name"];
		$new_adress = "my_images/".time().$name;
		if(!file_exists("my_images")){
			mkdir("my_images");
		}
		move_uploaded_file($temp, $new_adress);
		$this->insert("image", ["my_id"=>$id,"image"=>$new_adress]);
		header("location: photos.php");
	}
	# select all my pictures
	function sel_my_pics() {
		$id = $_SESSION["user"]["id"];
		$data = $this->db->query("SELECT * FROM image WHERE my_id = '$id'")->fetch_all(true);
		print(json_encode($data));

	}
	# set_my_pics set my profil photo
	function set_my_pics() {
		$id = $_SESSION["user"]["id"];
		$img_id = $_POST["img_id"];
		$data = $this->db->query("SELECT image from image WHERE id = '$img_id'")->fetch_all()[0][0];
		$this->update("user", ["photo"=>$data], ["id"=>$id]);
		$_SESSION["user"]["photo"] = $data;
		
	}
	#delete an photo in my photos
    function del_my_pics() {
    	$id = $_SESSION["user"]["id"];
		$img_id = $_POST["img_id"];
		$data = $this->db->query("SELECT image from image WHERE id = '$img_id'")->fetch_all()[0][0];
		unlink($data);
		$this->db->query("DELETE FROM image WHERE id = '$img_id'");

    }
    #get friend information
    function get_fr_info() {
    	$fr_id = $_POST["fr_id"];
    	$data = $this->db->query("SELECT name, surname, photo FROM user WHERE id = '$fr_id'")->fetch_all(true);
    	print json_encode($data);
    }

    #add_comment
    function add_comment() {
    	$my_id   = $_SESSION["user"]["id"];
    	$post_id = $_POST["post_id"];
    	$value   = $_POST["value"];    
    	$this->insert("comment", ["user_id"=>$my_id, "post_id"=>$post_id, "comment"=>$value]);
    }
    # add like
    function add_like() {
		$my_id = $_SESSION["user"]["id"];
    	$post_id = $_POST["post_id"];
    	$flag = $_POST["flag"];
    	if($flag == "like"){
    		$this->insert("`like`", ["user_id"=>$my_id, "post_id"=>$post_id]);
    	} else $this->delete("`like`", ["user_id"=>$my_id, "post_id"=>$post_id]);
    	print $my_id;
    }
    # get friends photos
    function get_fr_photos() {
    	$fr_id = $_POST["fr_id"]; 
    	$all_data = [];   	
    	$data  = $this->db->query("SELECT id,image FROM image WHERE my_id = '$fr_id'")->fetch_all(true); 
    	foreach($data as $value){
    		$value["likes"] =  $this->db->query("SELECT * FROM like_img join
					user on user.id = like_img.user_id
    		 WHERE like_img.image_id = '{$value["id"]}'")->fetch_all(true);

			$value['comments'] = $this->db->query("SELECT user.id, name,surname, photo, comment, time FROM
			 user JOIN comment_img ON user.id = comment_img.user_id	WHERE comment_img.image_id = '{$value["id"]}'")->fetch_all(true);
    		$all_data[] = $value;
    	}
    	print json_encode($all_data);
    }
    #like friend photos
    function to_like_img (){
    	$my_id = $_SESSION["user"]["id"];
    	$img_id = $_POST["img_id"];
    	$troy = $_POST["troy"];
    	if($troy == "true"){
    		$this->insert("like_img", ["user_id"=>$my_id, "image_id"=>$img_id]);
    	} else $this->delete("like_img", ["user_id"=>$my_id, "image_id"=>$img_id]);
    	print($my_id);
    }
    # add friend`s image commennt
    function add_img_comm() {
    	$user_id = $_POST["user_id"];
    	$my_id  = $_SESSION["user"]["id"];
    	$img_id = $_POST["img_id"];
    	$value  = $_POST["value"];
    	$this->insert("comment_img", ["user_id"=>$my_id, "image_id"=>$img_id, "comment"=>$value]);
    // 	$data = $this->db->query("SELECT user.id, name,surname, photo, comment, time FROM
	 		// user JOIN comment_img ON user.id = comment_img.user_id	WHERE comment_img.image_id = '{$user_id}' ORDER BY
	 		// time DESC")->fetch_all(true);
    // print($data);
    }

    # add event
    function event_form_send() {
    	var_dump($_POST);
    	$id = $_SESSION["user"]["id"];
    	$ev_name = $_POST["event_name"];
    	$event_place = $_POST["event_place"];
    	$event_descrip = $_POST ["event_descrip"];
    	$event_date_start = $_POST["event_date_start"];
    	$event_time_start = $_POST["event_time_start"];
    	$event_date_end = $_POST["event_date_end"];
    	$event_time_end = $_POST["event_time_end"];
    	$time_st  = "$event_date_start"."-"."$event_time_start";
    	$time_end = $event_date_end."-".$event_time_end;
    		$new_adress =  "";   		
    	if(!empty($_FILES["img_event"]["name"])){
    		print_r($_FILES);
    		$temp = $_FILES["img_event"]["tmp_name"];
    		$name = $_FILES["img_event"]["name"];
    		$new_adress = "images_ev/".time().$name;
    		if(!file_exists("images_ev")){
				mkdir("images_ev");
			} 
			move_uploaded_file($temp, $new_adress);
		} 
		else {
			$new_adress = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTHx7PC2ckLdzqy2VvWXdVtljsEhAbsWwQIFvznFYxBaHE8MOqxXA";
		}
		$this->insert("event", ["user_id"=>$id, "ev_name"=>$ev_name, "place"=>$event_place, "disc"=>$event_descrip,"time_start"=> $time_st, "time_end"=>$time_end,"fr_photo"=>$new_adress]);
    	header("location:profile.php");
    }
    #show event
    function show_event() {
    	$id = $_SESSION["user"]["id"];
    	$inf = [];
    	$data = $this->db->query("SELECT * FROM event WHERE user_id = '$id'")->fetch_all(true);
    	foreach ($data as $key) {
    		$id = $key["id"];
    		$key["interest"] = $this->db->query("SELECT user.name,surname, user.photo FROM user JOIN ev_interest
				ON user.id = ev_interest.user_id join event 
				ON event.id = ev_interest.event_id WHERE event.id = '$id'")->fetch_all(true);
    		$key["accept"] = $this->db->query("SELECT user.name,surname, user.photo FROM user JOIN ev_accept
				ON user.id = ev_accept.user_id join event 
				ON event.id = ev_accept.ev_id WHERE event.id = '$id'")->fetch_all(true);
    		$key["like"] = $this->db->query("SELECT user.name,surname, user.photo FROM user JOIN ev_like
				 ON user.id = ev_like.user_id join event 
				 ON event.id = ev_like.ev_id WHERE event.id = '$id'")->fetch_all(true);
    		$inf[] = $key;
    	}
    	print json_encode($inf);
    }
    #get friend events
    function get_fr_events() {
    	$my_id = $_SESSION["user"]["id"];
    	$fr_data = [];
    	$data = $this->db->query("SELECT event.*,user.name,user.surname,user.photo FROM 
			event JOIN user 
			ON user.id = event.user_id WHERE user_id in(
			SELECT my_id from friend where user_id = '$my_id'
				UNION
			SELECT user_id from friend where my_id = '$my_id')")->fetch_all(true);
    	foreach ($data as $key) {
    		$id = $key["id"];
    		$key["interest"] = $this->db->query("SELECT user.id,user.name,surname, user.photo FROM user JOIN ev_interest
				ON user.id = ev_interest.user_id join event 
				ON event.id = ev_interest.event_id WHERE event.id = '$id'")->fetch_all(true);
    		$key["accept"] = $this->db->query("SELECT user.id,user.name,surname, user.photo FROM user JOIN ev_accept
				ON user.id = ev_accept.user_id join event 
				ON event.id = ev_accept.ev_id WHERE event.id = '$id'")->fetch_all(true);
    		$key["like"] = $this->db->query("SELECT user.id, user.name,surname, user.photo FROM user JOIN ev_like
				 ON user.id = ev_like.user_id join event 
				 ON event.id = ev_like.ev_id WHERE event.id = '$id'")->fetch_all(true);

    		$fr_data[] = $key;
    	}
    	print json_encode($fr_data);
    }
    #add like friend
    function add_ev_dislike() {
    	$id = $_SESSION["user"]["id"];
    	$event_id = $_POST["event_id"];
    	$this->delete("ev_like", ["user_id"=>$id, "ev_id"=>$event_id]);
    	print $event_id;
    }
    function add_ev_like() {
    	$id = $_SESSION["user"]["id"];
    	$event_id = $_POST["event_id"];
    	$this->insert("ev_like", ["user_id"=>$id, "ev_id"=>$event_id]);
    	print $event_id;
    }
    #accept for event
    function add_ev_accept() {
    	$id = $_SESSION["user"]["id"];
    	$event_id = $_POST["event_id"];
    	$this->insert("ev_accept", ["user_id"=>$id, "ev_id"=>$event_id]);
    	$this->delete("ev_interest", ["user_id"=>$id, "event_id"=>$event_id]);
    }
    #Cancel event
    function cancel_events() {
    	$id = $_SESSION["user"]["id"];
    	$event_id = $_POST["event_id"];
		$this->delete("ev_accept", ["user_id"=>$id, "ev_id"=>$event_id]);
    	$this->delete("ev_interest", ["user_id"=>$id, "event_id"=>$event_id]);
    }
    # add event interest
    function add_ev_interest() {
    	$id = $_SESSION["user"]["id"];
    	$event_id = $_POST["event_id"];
    	$this->insert("ev_interest", ["user_id"=>$id, "event_id"=>$event_id]);
    	$this->delete("ev_accept", ["user_id"=>$id, "ev_id"=>$event_id]);
    }
}
$log = new Login();


