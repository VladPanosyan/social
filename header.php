<?php 
  session_start();
  if(isset($_SESSION["user"])){
    $user = $_SESSION["user"];
  }
  else{   
    header("location:index.php");
  }
 ?>
 
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <link rel="stylesheet" href="css/prof.css">
  <link rel="stylesheet" href="css/pass.css">
  <link rel="stylesheet" href="css/my_pic.css">
  <link rel="stylesheet" href="css/center.css">
  <link rel="stylesheet" href="css/guest.css">


  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <script src="js/my_pic.js"></script>
	<script src="js/profile.js"></script>
	<script src="js/sign.js"></script>
	<script src="js/pass.js"></script>
</head>
<body>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
  <!-- Brand -->
  <a class="navbar-brand" href="profile.php">My Blog</a>

  <!-- Links -->
  <ul class="navbar-nav ul">
    <li class="nav-item">
      <a class="nav-link" href="photos.php">Իմ նկարները</a>
    </li>
    <li class="nav-item">
      <a class="nav-link friend_list" href="#">Ընկերներ</a>
    </li>
    
    <li class="nav-item amount" style="position: relative">
      <i class="fas fa-bell fas1" style="color:blue; font-size: 30px;padding: 5px 3px 0; cursor: pointer"></i> 
    </li>
     <li class="nav-item msg_msg_info" style="position: relative">
      <i class="fas fa-envelope msg_info" style="color: white"></i> 
      <span></span>
    </li>
    
    <!-- Dropdown -->
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
        Այլ
      </a>
      <div class="dropdown-menu">
        <a class="dropdown-item change_self_data" href="#">Փոխել անձնական տվյալները</a>
        <a class="dropdown-item change_pass" href="password.php">Փոխել գաղտնաբառը</a>
      </div>
    </li>
    
    <div class="form-inline" action="server.php">
    	<input class="search form-control mr-sm-2" type="text" placeholder="Search">
    <button class="btn btn-success" type="submit">Search</button>
  </div>
  <span style="height: 40px; width: 2px; background: #fff;margin-left: 5px"></span>
   <li class="nav-item">
      <div class="nav-link log_out">LogOut</div>
    </li>
    
  </ul>
</nav>
<div class="ch_data" style="display: none">
    <label for="">Name</label>
    <input type="text" id="name" value="<?=$user["name"] ?>">
    <label for="">Surname</label>
    <input type="text" id="surname" value="<?=$user['surname']?>">
    <label for="">Age</label>
    <input type="text" id="age" value="<?=$user['age']?>">
    <label for="">Email</label>
    <input type="text" id="email" value="<?=$user['email']?>">
    <button class="edit btn btn-warning" style="margin-top: 5px">Edit</button>
    <button class="edit_exit btn btn-info" style="margin-top: 5px">Դուրս գալ</button>
  </div>
  <div class="searching">
    <div class="searches1" style="display: none;"></div>
    <!-- <div class="friend_view friend_view_hide"></div>  --> 
  </div>
  <input type="hidden" id="my_id" value="<?=$user['id'] ?>">
  