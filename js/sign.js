$(document).ready(function() {
	$('.inner').animate({ 
		'left': 0
	});
	function placeholder() {
		$("form input").addClass('pl');	
	}	
	setTimeout(placeholder, 500);
	$(".reg").slideToggle(400, function() {
		$(".reg label").each(function(index, item) {
			setTimeout(function() {
				$(item).css("opacity", "1");
			}, index * 60)					
		});
	});

	// *** ajax validate *** 
	
	$(".register").click(function() {
		let name     = $("#name").val();
		let surname  = $("#surname").val();
		let email    = $("#email").val();
		let age      = $("#age").val();
		let password = $("#password").val();
		let confPass = $("#confPass").val();
		$.ajax({
			url: "server.php",
			type: "POST",
			data: {
				action   : "validate",
			 	name     : name,
			  	surname  : surname,
			  	email    : email,
			  	age      : age,
			  	password : password,
			  	conf_pass: confPass
				},
			success: function(req) {
				if(req === 'true'){
					$('.reg input').val("");
					let p = $("<p>Դուք հաջողությամբ գրանցվել եք</p>").css({
						color: "blue",
						fontSize: "16px",
						fontStyle: "italic",
						textAlign : "center"
					});
					p.appendTo(".reg");
					$("form").fadeOut(500, function() {
						$("form").addClass('transport');
						$("body").append('<div class="bg_log"></div>');
						$("form").prepend('<h3 style="text-align: center; color: #FFF ">Մուտքագրեք Ձեր գաղտնաբառը</h3>');
						$("form").fadeIn(500);						
					});			
					$(".alert").remove();
				}else{
					if(req.trim()){
						$(".alert").remove() ;
						req = JSON.parse(req);
						for(let key in req){
							$("#" + key).before(`<div class="alert">${req[key]}</div>`);
							$(".alert").css("color","red");
						};										
					}
				}
			}
		});
	});
});