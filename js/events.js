$(document).ready(function() {
	$(".btn_add").click(function(event) {
		$(".hid_ev_reg").toggleClass('hid_ev_reg_sh');
		if($(".hid_ev_reg").hasClass('hid_ev_reg_sh')){
			$(".hid_ev_reg").show();
			$("body").css("overflow", "hidden");
		} else $(".hid_ev_reg").hide();
	});

	(function getDate(){
		    var today = new Date();
		    let date = today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2);
			$(".event_date_start,.event_date_end").val(date);
			$(".event_time_start").val("00:00");
			$(".event_time_end").val("00:00");
		})();
		$(".exit_ev_ev").click(function(event) {
			$(".hid_ev_reg").hide();
			$(".hid_ev_reg").toggleClass('hid_ev_reg_sh');
		});
// event submit
	$(document).on("click", ".event_form_send", function(e){
		e.preventDefault();
		if($(".event_name").val() && $(".event_place").val() && $(".event_descrip ").val()){
  			$(".fL_ev").submit();
		}		
	});
// add event 
	function show_event() {
	 	$.ajax({
			url: 'server.php',
			type: 'POST',
			data: {action: 'show_event'},
			success: function(r) {
				r = JSON.parse(r);
				console.log(r);
				r.forEach(function(item) {
					let el = $(`
							<div class="ev_list d-flex flex-column">
								<div class="ev_info d-flex justify-content-between mb-3">
									<div class="fr_ev_img">
										<img src="${item.fr_photo}" alt=""/>
									</div>
									<table class="evnt_ev d-flex flex-column flex-fill">
										<tr>
											<td>Անվանում</td>
											<td>${item.ev_name}</td>
										</tr>
							 			<tr>
							 				<td>Վայրը</td>
							 				<td>${item.place}</td>
							 			</tr>
							 			<tr>
								 			<td>Նկարագրությունը</td>
								 			<td>${item.disc}</td>
							 			</tr>
							 			<tr>
								 			<td>Սկիզբը</td>
								 			<td>${item.time_start}</td>
							 			</tr>
							 			<tr><td>Ավարտը</td><td>${item.time_end}</td></tr>
							 			<tr>
								 			<td><textarea placeholder="Comment . . ." id="ev_comm"></textarea></td>
								 			<td><button id="${item.id}">Comments</button></td></tr>
									</table>
								</div>
								<div class='ev_ev_lk d-flex justify-content-between'>
									<p id="${item.id}">Հետաքրքրվում են<span>${item.interest.length}</span></p>
									<p id="${item.id}">Գալու են <span>${item.accept.length}</span></p>
									<p id="${item.id}">LIKE <span>${item.like.length}</span></p>
								</div>
							
						</div>`);
					$(".ev_contain").append(el);
				});
			}
		});
	};
	show_event();
	// my events
	$(".my_ev").click(function() {
		$(".ev_contain").empty();
		show_event();
	});
	// frien events
	function show_fr_ev() {
		let my_id = $("#my_id").val();
		$.ajax({
			url: "server.php",
			type: "POST",
			data: {action: "get_fr_events"},
			success: function(r) {
				r = JSON.parse(r);
				console.log(r);
				r.forEach(function(item) {
					let flag = item.like.some(e=>e.id == my_id);
					let flagAccept = item.accept.some(e=>e.id == my_id);
					let flagInterest = item.interest.some(e=>e.id == my_id);let flagInterest1 = item.interest.some(e=>e.id == my_id);					
					let el = $(`
						<div class="ev_list d-flex flex-column">	
							<div class="fr_ev_inf d-flex justify-content-between mt-2 ml-2">
								<div class="imgg d-flex"><img src="${item.photo}" alt="" /></div>
								<p>${item.name}</p>
								<p>${item.surname}</p
								<p>${item.time}</p>	
							</div>
							<div class="ev_info d-flex justify-content-between mb-3">
								<div class="fr_ev_img">
									<img src="${item.ev_photo}" alt=""/>
								</div>
								<table class="evnt_ev d-flex flex-column flex-fill">
									<tr>
										<td>Անվանում</td>
										<td>${item.ev_name}</td>
									</tr>
						 			<tr>
						 				<td>Վայրը</td>
						 				<td>${item.place}</td>
						 			</tr>
						 			<tr>
							 			<td>Նկարագրությունը</td>
							 			<td>${item.disc}</td>
						 			</tr>
						 			<tr>
							 			<td>Սկիզբը</td>
							 			<td>${item.time_start}</td>
						 			</tr>
						 			<tr><td>Ավարտը</td><td>${item.time_end}</td></tr>
						 			<tr>
							 			<td><textarea placeholder="Comment . . ." id="ev_comm"></textarea></td>
							 			<td><button id="${item.id}">Comments</button></td></tr>
								</table>
							</div>
							<div class='ev_ev_lk d-flex justify-content-between'>
								<div class="int d-flex flex-column">
${(flagInterest) ? `<button id="${item.id}" disabled>Հետաքրքրում է</button>` : `<button id="${item.id}">Հետաքրքրում է</button>`}
									<i class="fas fa-users"><span>${item.interest.length}</span></i>
								</div>
								<div class="acp d-flex flex-column">
${(flagAccept) ? `<button id="${item.id}" disabled>Գալու եմ</button>` : `<button id="${item.id}">Գալու եմ</button>`}
									<i class="fas fa-users"><span>${item.accept.length}</span></i>
								</div>
								<div class="cancel d-flex flex-column">
${(flagAccept || flagInterest) ? `<button id="${item.id}" class="">Չեղարկել</button>` :  `<button id="${item.id}" class="" disabled>Չեղարկել</button>`}								
								</div>
								<div class="acpp d-flex flex-column">
${(flag) ? `<i class="btn btn-danger btn-sm ev_dislike" id="${item.id}" data-user="${item.user_id}">Dislike</i>` : `<i class="ev_like btn btn-info btn-sm" id="${item.id}" data-user="${item.user_id}">Like</i>`}
									<span>${item.like.length}</span>
								</div>
							</div>						
					</div>`);
					$(".ev_contain").append(el);
				});			
			}
		});
	}
	$(".fr_ev").click(function() {
		$(".ev_contain").empty();
		show_fr_ev();
	});
	// event like
	$(document).on("click", ".ev_like", function() {
		let event_id = $(this).attr("id");
		console.log(event_id);
		let val = +$(this).next().html();
		$(this).html("Dislike");
		$(this).next().html(val + 1);
		$(this).addClass('ev_dislike btn btn-danger').removeClass('ev_like btn btn-info');
		$.ajax({
			url    : "server.php",
			type   : "POST",
			data   : {action: "add_ev_like",event_id: event_id},
			success: function(r) {
				console.log(r);
			}

		});

	});
	// event dislike
	$(document).on("click", ".ev_dislike", function() {
		let val = +$(this).next().html();
		let event_id = $(this).attr("id");
		$(this).html("Like");
		$(this).addClass('ev_like btn btn-info').removeClass('ev_dislike btn btn-danger');
		$(this).next().html(val - 1);
		$.ajax({
			url    : "server.php",
			type   : "POST",
			data   : {action: "add_ev_dislike",event_id: event_id},
			success: function(r) {
				console.log(r);
			}
		});
	});
	//event come
	$(document).on("click", ".acp button", function() {
		$(this).attr("disabled", true);
		$(this).parents(".ev_ev_lk").find(".cancel button").attr("disabled", false);
		
		let val  = +$(this).next("i").find("span").html();
		let val1 = +$(this).parents(".ev_ev_lk").find(".int span").html();
		
		$(this).next("i").find("span").html(val + 1);
		if($(this).parents(".ev_ev_lk").find(".int button").is(":disabled")) {
			$(this).parents(".ev_ev_lk").find(".int span").html(val1 - 1);
		}  
		$(this).parents(".ev_ev_lk").find(".int button").attr("disabled", false);
		let event_id = $(this).attr("id");
		$.ajax({
			url    : "server.php",
			type   : "POST",
			data   : {action: "add_ev_accept",event_id: event_id},
			success: function(r) {				
			}
		});
	});
	// interest event
	$(document).on("click", ".int button", function() {
		$(this).attr("disabled", true);
		$(this).parents(".ev_ev_lk").find(".cancel button").attr("disabled", false);
		
		let val = +$(this).next("i").find("span").html();
		$(this).next("i").find('span').html(val + 1);
		let val1 = +$(this).parents(".ev_ev_lk").find(".acp span").html();
		if($(this).parents(".ev_ev_lk").find(".acp button").is(":disabled")) {
			$(this).parents(".ev_ev_lk").find(".acp span").html(val1 - 1);
		} 
		$(this).parents(".ev_ev_lk").find(".acp button").attr("disabled", false);
		let event_id = $(this).attr("id");
		$.ajax({
			url    : "server.php",
			type   : "POST",
			data   : {action: "add_ev_interest",event_id: event_id},
			success: function(r) {
			}
		});
	});
	// cancel event
	$(document).on("click", ".cancel button", function() {
		let val = +$(this).parents(".ev_ev_lk").find("button:disabled").next('i').find("span").html();
		$(this).parents(".ev_ev_lk").find("button:disabled").next("i").find("span").html(val-1)
		$(this).parents(".ev_ev_lk").find("button:disabled").prop("disabled", false);
		$(this).prop("disabled", true);
		let event_id = $(this).attr("id");
		$.ajax({
			url    : "server.php",
			type   : "POST",
			data   : {action: "cancel_events",event_id: event_id},
			success: function(r) {
			}
		});
	});
	// event comment 
$(document).on("keypress", "#ev_comm", function(event) {
	if(event.key == "Enter"){
		alert(5);
	}
});
});
  