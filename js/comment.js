// add comment in friend post
	$(document).on("keypress", ".comment", function(e) {
		let self = $(this);
		if (e.keyCode == 13 && !!$(this).val().trim()) {			
			let post_id = $(this).parents(".one").attr("id");		
			let value   = $(this).val().trim();//encoded()
			$.ajax({
				url: "server.php",
				type: "POST",
				data: {action: "add_comment", post_id: post_id, value:value},
				success: function(r) {
					console.log(r);
					self.val("");
				}

			});
			e.preventDefault();
		}
	});