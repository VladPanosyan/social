 $(document).ready(function() {
 	// going to guest 
 	$(".searching").on("click", ".biznes_class + span", function() {
 		let id = $(this).parent().find("li:last button").attr("id");
 		location.href = `guest.php?id=${id}`;
 	});
 	$(".searching").on("click", ".req_info", function() {
 		let id = $(this).prev("li").find("button").attr("id");
 		location.href = `guest.php?id=${id}`;
 	});

// detect users becoming in offline add_to_ofline
	// if(location){

	// }
	$(".log_out").click(function() {
		$.ajax({
		url : "server.php",
		type: "POST",
		data: {action: "add_to_ofline"},
		success: function(r){
			location.href = 'index.php';
			}
		});
	});

 	//ajax request for posts 

 	$.ajax({
 		url: "server.php",
			type: "POST",
			data: {action: "get_posts"},
			success: function(r) {
				r = JSON.parse(r);
				localStorage.photo =r[0].photo ;
				console.log(r)
				let id = $("#my_id").val();
				console.log(r);
				r.forEach(function(elem, index) {
					let source = "";
					let data = "";
					let comments = ``;
					let img = ``;
					let  k = elem.likes.some(a=> a.user_id == id);
					elem.comments.forEach(function(item){
						comments+=`
							<div class="coment_res">
								<ul class="com_inf">
									<li><img src="${item.photo}" alt=""></li>
									<li><a href="guest.php?id=${item.id}">${item.name} ${item.surname}</a></li>
								</ul>
								<div class="com_source">
									<p class="p1">${item.comment}</p>
									<p class="p2">${item.time}</p>
								</div>
							</div>
						`;
					})
					if(elem.post_img){
						img = elem.post_img;
					} 
					if(elem.post_video){
						source = elem.post_video;
					}
					let el = $(`
							<div class="post_result1">
								<div class="head_inf">
									<img src="${elem.photo}">
									<div class="about_fr pl-2">							
										<h4>${elem.name} ${elem.surname}</h4>
										<p>${elem.time}</p>
									</div>
								</div>
								<div class="one" id="${elem.id}">									
									<div class="img_plus_post">
${(img) ? `<img src="${img}" alt="">` : (source) ? `<video controls class="vid">
														<source src="${elem.post_video}" type="video/mp4" >
													</video>` : null}
										<p>${elem.post}</p>
									</div>
									<div class="coment_pl_like p-2">
										<ul>
											<li><i class="far fa-thumbs-up"></i><span class="likes">${elem.likes.length}</span></li>
											<!-- <li></li>
											<li></li>
											<li></li> -->
										</ul>
							
										${(k)?`<button class="btn btn-danger dislike"><i class="fas fa-thumbs-down"></i></button>`:`<button class="btn btn-success like"><i class="fas fa-thumbs-up"></i></button>`}
										<textarea name="" id="comment" cols="30" rows="10" class="comment pl-4" placeholder="Add Your comment . . ."></textarea>
									</div>
									<div class="comments_btn"><i class="show_coments fas fa-arrow-alt-circle-down" data-toggle="popover" data-placement="right" data-content="No-Comments"></i></div>
									<div class="all_comments" style="display:none">${comments}</div>
								</div>							
							</div>
						`);
					$(".posts").append(el);	
					$(".img_plus_post img[src='']").remove();			
				});
			}
 	});



 	$(".prof_pic").change(function() {
 		$(".my_form").submit();
 	});

 	$('.change_self_data').click(function() {
 		$(".inctance_bg").slideDown(300);
 		$(".ch_data").slideDown(500);
 	});

 	$('.edit_exit').click(function() {
 		$(".inctance_bg").slideUp(300);
 		$(".ch_data").slideUp(500);
 	});

 	$('.edit').click(function() {
		let data = {}
		$(".ch_data input").each((index,elem)=>{
			elem = elem.value.trim();
			let changed = $(".ch_data label").eq(index).text().toLowerCase();
			data[changed] = elem;
		});
		console.log(data);
 		
 		$.ajax({
 			url: "server.php",
 			type: "POST",
 			data: {action: "edit_user_data", changed_self_data: data},
 			success: function(r) {
 				if(r == "true"){
 					alert("tvyalner poxvats en")
	 					$(".ch_data").slideUp(800, function() {
	 					location.reload(); 					
 					});
 				} else {
 					r = JSON.parse(r);
 					for(let key in r){
 						$(`#${key}`).after(`<p>${r[key]}<p>`).css({
 							"color": 'red',
 							"fontStyle": 'italic'
 						});
 					}
 				}

 				
 				$(".inctance_bg").hide();
 			}
 		});
 	});

 	// search all users

 	$(".search").on("input", function() {
 		let val = $(this).val();
 		// $(".searching .searches1").hide();
 		$.ajax({
 			url : "server.php",
 			type: "POST",
 			data: {action: "search", value: val},
 			success: function(r) {
 				$(".searches").remove();
	 				// $(".search").blur(function() {
	 				// 	$(".searches").empty();
	 				// });
	 				r = JSON.parse(r);
	 				console.log(r);							
				if(val && r.length) {
					//console.log(r);
					let el = $("<div class=\"searches\"></div>");
					r.forEach( function(elem, index) {
						let btn = `<button class="add_fr btn btn-success"id="${elem.id}" data-inf= "${elem.status}"><i class="far fa-address-book"></i></button>`
						let clas='';
						let data = '';
						// let id = `${elem.id}`;
						if (elem.status=="friend") {
							btn =`<button class="btn btn-danger btn-sm del_fr" id="${elem.id}" data-inf= "${elem.status}">Remove</button>`
						}
						else if (elem.status=="my_request") {
							btn = `<button class="add_fr_remove btn btn-danger" id="${elem.id}" data-inf= "${elem.status}"><i class="far fa-address-book"></i></button>`;
							// id = elem.id;

						}
						else if (elem.status=="user_request") {
							btn = `<button class="btn btn-info btn-sm accept kkk" id="${elem.id}">Accept</button>
							<button class="btn btn-danger btn-sm no_accept kkk" id="${elem.id}">Remove</button>`
						}else if(elem.status == "my_id"){
							clas = 'you';
						}

						$(el).append(`		
							<ul class="inner ${clas}">
								<li><a href="guest.php?id=${elem.id}">${elem.name} ${elem.surname}</a></li>
								<li><img src="${elem.photo}"></li>
								<li>${btn}</li>
							</ul>				
						`);
					});	
					$(".searching").append(el);
					$("button[data-inf=my_request]").removeClass('btn-access add_fr');
					$("button[data-inf=my_request]").addClass('btn-danger add_fr_remove ggg');
					$("button[data-inf=my_request]").parents(".inner").append(`<li class="req_info"><span>Հարցումը ուղարկված է</span></li>`);		
					$("button[data-inf=friend]").addClass('btn-danger del_fr');
					$("button[data-inf=friend]").parents("ul").children("li:nth-child(1)").addClass("biznes_class").after("<span>Ընկեր<span>");
					$(`.you`).remove();
				} 	 				
 			}
 		});		
 	});
 	$(document).on("focus", ".search", function() {
 		$(".searching>div:not(.searches1)").empty();
 		$(".far1").addClass('fas fas1').removeClass('far far1');
 		$(".activeMsg").addClass('msg_info').removeClass('activeMsg');
 	});
 	$(document).on("blur", ".search", function() {

 		// $(this).val('');
 		// $(".searches").remove();
 		
 	});
 	// add REQUEST
	$(document).on("click",".add_fr", function() {
		$(this).parents(".inner").append(`<li class="req_info"><span>Հարցումը ուղարկված է</span></li>`);
		let userId = $(this).attr("id");
		$(this).after(`<button class="add_fr_remove btn btn-danger" id="${userId}"><i class="far fa-address-book"></i></button>`).remove();
	 	$(this).parents(".inner").append(`<li class="sss"><span>Հարցումը ուղարկված է</span></li>`);
	 	
		$.ajax({
			url : "server.php",
			type: "POST",
			data: {action: "add_request", user_id: userId},
			success: function(r){
				// console.log(r);
			}
		});
	});
	// remove request
	$(document).on("click",".add_fr_remove", function() {
		let userId = $(this).attr("id");
		$(this).parent("li").next().remove();
		$(this).after(`<button class="add_fr btn btn-success" id="${userId}"><i class="far fa-address-book"></i></button>`).remove();
		$.ajax({
			url : "server.php",
			type: "POST",
			data: {action: "remove_request", user_id: userId},
			success: function(r){
			}
		});

	});

	// get requests
	function getRequests() {
		$.ajax({
			url : "server.php",
			type: "POST",
			data: {action: "get_requests"},
			success: function(r) {
			
		 	$(".searches1").empty();
		 	$(".searches").remove();
			$(".friend_view_hide").remove();			
				r = JSON.parse(r);
				if(r.length){
					$(".amount").append(`<span style="position: absolute; color: red; top: -7px; right: 2px">${r.length}</span>`);				
				r.forEach(function(elem, index) {
					$(".searches1").append(`
						
							<ul class="req_inner req">
								<li><a href="guest.php?id=${elem.id}">${elem.name} ${elem.surname}</a></li>
								<li><img src="${elem.photo}"></li>
								<li>
									<button class="btn btn-info btn-sm accept" id="${elem.id}">Accept</button>
									<button class="btn btn-danger btn-sm no_accept" id="${elem.id}">Remove</button>
								</li>
								
							</ul>
										
						`);
					})	
					// $(".searches1").fadeToggle();							
				}
				 else {
					$(".searches1").append("<div class=\"empty_msg\" style=\"display: none\">Ձեզ Ընկերանալու հայտ ոչ-ոք չի ուղարկել</div>");
				}
			}

		});
	}
	getRequests();
// show all request
		$(document).on("click", ".fas1", function() {

			getRequests();
			$(".searches1").show();	
			$(this).after(`<i class="far fa-bell far1" style="color:blue; font-size: 30px;padding: 5px 3px 0; cursor: pointer"></i>`).remove();
			$(".friend_view_hide").remove();
			$(".msg_show").remove();
			$(".activeMsg").addClass('msg_info').removeClass('activeMsg');

	});
// Hide all requests
		$(document).on("click", ".far1", function() {
			$(this).after(`<i class="fas fa-bell fas1" style="color:blue; font-size: 30px;padding: 5px 3px 0; cursor: pointer"></i>`).remove();	
			$(".searches1").empty();
			$(".empty_msg").hide();
			

		});

// remoove freindship request(don`t get Friend)
	$("body").on("click", ".no_accept", function() {
		let id = $(this).attr("id");
		 $(this).closest('ul').remove();
		 $(".amount span").text( $(".amount span").text() -1);

		$.ajax({
			url : "server.php",
			type: "POST",
			data: {action: "remove_request_my", user_id: id},
			success: function(r){
			}
		})
	});
// Accept friendship request(get Friend)
	$("body").on("click", ".accept", function() {
		$(this).closest('ul').remove();
		$(".amount span").text( $(".amount span").text() -1);
		let id = $(this).attr("id");
		console.log(id);
		$.ajax({
			url : "server.php",
			type: "POST",
			data: {action: "add_friend", user_id: id},
			success: function(r){
				console.log(r);
			}
		})
	});
	// get friends
	function getFriends(){
		$.ajax({
			url : "server.php",
			type: "POST",
			data: {action: "get_friend"},
			success: function(r){
				$(".searching>div:not(.searches1)").remove();
				r = JSON.parse(r);
				console.log(r);
				let d = $("<div class=\"friend_view_hide\"></div>");
				d.prepend("<input class=\"friend_search\" placeholder=\"Find friends...\">");
				r.forEach(function(elem) {
					d.append(`
						<ul class="req_inner fr">
							<li><a href="guest.php?id=${elem.id}">${elem.name} ${elem.surname}</a></li>
							<li class="fr_msg_info" >
								<button class="btn btn-danger btn-sm del_fr" id="${elem.id}">Remove</button>
								<button class="btn btn-info btn-sm  open_msg" id="${elem.id}"><i class="far fa-envelope" style="font-size:20px"></i></button>						
							</li>
							<li><img src="${elem.photo}"></li>
						</ul>
					`);
				});
				$(".searching").append(d);
			}
		});
	}
		
	$(".friend_list").click(function() {
		$(".activeMsg").addClass('msg_info').removeClass('activeMsg');		
		$(".searches1 .req").hide();
		$(".friend_view_hide").toggleClass('friend_view');
		$(".searches").remove();

		if(!$(".friend_view").hasClass('friend_view_hide')){
			getFriends();
		}else 	$(".searches1").empty();

		if($(".amount > i").hasClass('far1')){
			$(".amount > i").removeClass('far far1').addClass('fas fas1');
		}			
	});

	// delete friend on FriendList
	$("body").on("click", ".del_fr", function() {
		let id= $(this).attr("id");
		$(this).parents("ul").remove();
		$.ajax({
			url : "server.php",
			type: "POST",
			data: {action: "del_friend", friend_id: id},
			success: function(r){
				$(".ul1").find(`#${id}`).parents(".ul1").remove();
				console.log(r);
			}
		});
	});
	// Open Message window AND Get all messages
	// incoming message click
	let stop;
	$(document).on("click", ".open_msg", function(){
		$(".searching").empty();
		$(".activeMsg").addClass('msg_info').removeClass('activeMsg');
		let adr    = $(this).parents("ul").find("img").attr("src");
		let adr_my = localStorage.photo;
		console.log(adr_my)
		let name   = $(this).parents("ul").find("li:first").text();
		let id     = $(this).parents("ul").find("button").attr("id");
		console.log(name);
		$(".friend_view").fadeOut(300);
		$(".friend_view").addClass("friend_view_hide");
		$(".up img").attr("src", adr);
		$(".up p").html(name);
		$(".send .send_msg").attr("id", id);
		$(".msg").show();	
		getMessage(id,adr_my,adr)
		clearInterval(stop);
		//stop = setInterval(getMessage, 1000, id, adr_my, adr);
		let res = $(".msg_info + span").html();
		// $(".msg_msg_info span").html(+res - $(this).siblings('span').text());
	 // 	$(this).next().html("");
	 	$(".msg").show();
		$.ajax({
			url: "server.php",
			type: "POST",
			data : {action: "inc_msg", user_id: id},
			success: function(r) {	
			console.log(r);		
			}
		});
	});
	// function getMessages
	function getMessage(id,adr_my,adr){
		$.ajax({
			url : "server.php",
			type: "POST",
			data: {action: "get_messages", user_id: id},
			success: function(r){
				$(".sec1").empty()
				r = JSON.parse(r);
				console.log(r)
				r.forEach(function(elem) {
					if(elem.user_id == id){
						let time = new Date(+elem.time).toString().match(/.*\sG/g)[0].slice(0, -2);
						let div = $(`<div class="msg_10">
										<div class="msg_img">
											<img src="${adr_my}">
											<p>${elem.message}</p>
										</div>										
										<p>${time}</p>
									</div>`);
						$(".sec1").append(div);
					} else {
						let time = new Date(+elem.time).toString().match(/.*\sG/g)[0].slice(0, -2);
						let div = $(`<div class="msg_10 frfr">
										<div class="msg_img">
											<p>${elem.message}</p>
											<img src="${adr}">
										</div>										
										<p>${time}</p>
									</div>`);
						$(".sec1").append(div);
					}
				});	
				$(".sec1").get(0).scrollTop = $(".sec1").get(0).scrollHeight + 200;	
				console.log($(".sec1").get(0).scrollTop);			
			}
		})
	}
	// close message window
	$(".close_msg").click(function() {
		$(".msg").hide();
		
	});
	// 
	$(".inp").focus(function() {
		$(".sec").show();
	});
	$(".inp").blur(function() {
		$(".sec").hide();
	});
	$(".send").on("input", ".inp", function() {
		$(".sec").val($(this).val());
		$(".sec").show();
	});

	// send mesage
	$(".send_msg").click(function() {
		let msgVal = $(".sec").val();
		$(".inp").val("");
		$(".sec").val("");		
		let	id = $(this).attr("id");
		if(msgVal){
			$.ajax({
				url : "server.php",
				type: "POST",
				data: {action: "send_msg", user_id: id, msg_val: msgVal},
				success: function(r){
					console.log(r);
				}
			});
		}		
	});
	$(document).on("input focus", ".friend_search", function() {
		let value = $(this).val();		
		$.ajax({
			url : "server.php",
			type: "POST",
			data: {action: "find_friends", value: value},
			success: function(r){
				r = JSON.parse(r);
				console.log(r);
				$(".friend_view_hide ul").remove();
					// $(".friend_view").addClass('class_name');
					if(value){					
						r.forEach(function(elem) {
							$(".friend_view_hide").append(`
								<ul class="req_inner fr">
									<li><a>${elem.name} ${elem.surname}</a></li>								
									<li>
										<button class="btn btn-danger btn-sm del_fr" id="${elem.id}">Remove</button>
										<button class="btn btn-info btn-sm open_msg" id="${elem.id}"><i class="far fa-envelope" style="font-size:20px"></i></button>
									</li>
									<li><img src="${elem.photo}"></li>
								</ul>
							`);
						});
					}
				}
		});		
	});

	// delet friend from 'search'

	$(".searching").on("click",".del_fr_from_srch", function(){
		$(this).remove();
		$(".biznes_class + span").remove();
		$(".biznes_class").removeClass("biznes_class");
		let id = $(this).prev().attr("id");
		$.ajax({
			url   : "server.php",
			type  : "POST",
			data  : {action: "del_friend", friend_id: id},
			succes: function(r){
				console.log(r);
			}
		});
	});

	// New message Iformation

	//let stopMsg = setInterval(function() {
		$.ajax({
			url: "server.php",
			type: "POST",
			data : {action: "msg_count"},
			success: function(r) {
				console.log(r)
				if(r != 0){
				$(".msg_msg_info span").html(r);
				} 
			}
		});
	//},1000)

	$(document).on("click",".msg_info",function() {
		$(".far1").addClass('fas fas1').removeClass('far far1');
		$(".friend_view_hide").remove();
		$(".searches1").empty();
		$(".searches").remove();
		$(".search").val("");
		$.ajax({
			url: "server.php",
			type: "POST",
			data : {action: "get_all_messages"},
			success: function(r) {
				$(".msg_msg_info span").html("");
				$(".msg_info").toggleClass('activeMsg').removeClass('msg_info');
				$(".msg_show").remove();
					r = JSON.parse(r);
					console.log(r);
				if(r.length){
					let div = $(`<div class="msg_show"></div>`);
					r.forEach(function(elem, index) {
						$(div).append(`
							<ul class="req_inner fr">
								<li><a href=guest.php?id={elem.my_id}>${elem.name} ${elem.surname}</a></li>
								<li><img src="${elem.photo}"></li>
								<li class="msg_count">
									<button class="btn btn-info btn-sm incoming_msg open_msg online${elem.online}" id="${elem.my_id}"><i class="fas fa-comment"></i></button>
									<span class="count">${elem.count}</span>
								</li>
							</ul>
						`);
					});
					$(".searching").append(div);
				}else {
					let div = $(`<div class="msg_show"></div>`);
					div.append("<p>Դուք չունեք չկարդացած հաղորդագրություն</p>");	
					$(".searching").append(div);
				}	
			}
		});
	});


	$(document).on("click", ".activeMsg", function() {
		// $(this).after(`<i class="far fa-envelope msg_info" style=""></i>`).remove();
		$(".activeMsg").addClass('msg_info').removeClass('activeMsg');
		$(".msg_show").hide();
	});


	// share post (image or text)
	$(".top").on("click", ".share", function() {		
		let value = $(".pub textarea").val();
		$(".var").val(value);
		console.log($("#upl").val());
		if(!!value || !!$("#upl").val()){
			$(".info").html('GOOD !!!').fadeOut(1000,function() {$(".info").empty()});
			setTimeout(function() {
				$("#forma").submit();
			}, 600);
		}
	});
	$(".posts").on("click", ".show_coments", function() {
		$(this).parents(".one").find(".all_comments").slideDown(400);
		$(this).after("<i class=\"comment_hide fas fa-arrow-alt-circle-up\"></i>").remove();

	}) 
	$(".posts").on("click", ".comment_hide", function() {		
		$(this).parents(".one").find(".all_comments").slideUp(400);
		$(this).after("<i class=\"show_coments fas fa-arrow-alt-circle-down\"></i>").remove();

	});

	// to like
	$(document).on("click", ".like", function() {
		let val = +$(this).prev("ul").find("span").html();
		let postId = $(this).parents(".one").attr("id");
		$(this).prev("ul").find("span").html(val+1);
		$(this).after(`<button class="dislike btn btn-danger"><i class="fas fa-thumbs-down"></i></button>`).remove();
		$.ajax({
			url: "server.php",
			type: "POST",
			data: {action: "add_like", post_id: postId, flag: "like"},
			success: function(r) {
				console.log(r);
			}
		});


	});
	// dislike
	$(document).on("click", ".dislike", function() {
		let val = +$(this).prev("ul").find("span").html();
		let postId = $(this).parents(".one").attr("id");
		console.log(val);
		$(this).prev("ul").find("span").html(val-1);
		$(this).after(`<button class="like btn btn-success"><i class="fas fa-thumbs-up"></i></button>`).remove();
		$.ajax({
			url: "server.php",
			type: "POST",
			data: {action: "add_like", post_id: postId, flag: "dislike"},
			success: function(r) {
				console.log(r);
			}
		});
	});


 });
 // ALG 2 ic-C PARTADIR enter keypress message send Kanes bratan