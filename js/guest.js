$(document).ready(function() {
	// get frien imformation
	const fr_id = $("#fr_id").val();
	$.ajax({
		url: "server.php",
		type: "POST",
		data: {action: "get_fr_info", fr_id: fr_id},
		success: function(r) {
			r = JSON.parse(r);
			let el = $(`
				<img src="${r[0].photo}">
			`);
			$(".fr_img_wr").append(el);
			$(".fr_info").append(`
					<p>${r[0].name + " " + r[0].surname}</p>
				`);
		}
	});
	// get friend posts
	$.ajax({
		url: "server.php",
		type: "POST",
		data: {action: "get_fr_posts", fr_id: fr_id},
		success: function(r) {
			let id = $("#my_id").val();
			console.log(id)
			r = JSON.parse(r);
			console.log(r);
			r.forEach( function(elem, index) {
				let comments = ``;
				let  myLike = elem.likes.some(a=> a.user_id == id);
				elem.comments.forEach(function(item){
						comments+=`
							<div class="coment_res">
								<ul class="com_inf">
									<li><img src="${item.photo}" alt=""></li>
									<li><a href="#">${item.name} ${item.surname}</a></li>
								</ul>
								<div class="com_source">
									<p class="p1">${item.comment}</p>
									<p class="p2">${item.time}</p>
								</div>
							</div>
						`;
					})
				let img = "";
					let data = "";
					if(elem.post_img){
						img = elem.post_img;
					} 
					let el = $(`
						<div class="gen_post p-2">
							<div class="post_result1">
								<div class="one" id="${elem.id}">
									<div class="img_plus_post">
										<img src="${img}" alt="">
										<p>${elem.post}</p>
									</div>
									<div class="coment_pl_like">
										<ul>
											<li><i class="fas fa-thumbs-up"></i><span>${elem.likes.length}</span></li>
											<!-- <li></li>
											<li></li>
											<li></li> -->
										</ul>
${(myLike) ? `<button class="btn btn-danger dislike"><i class="fas fa-thumbs-down"></i></button>`:`<button class="btn btn-success like"><i class="fas fa-thumbs-up"></i></button>`}
										<textarea name="" id="comment" cols="30" rows="10" class="comment pl-2" placeholder="Add yor comment . . ."></textarea>
									</div>
									<div class="comments_btn"><i class="show_coments1 fas fa-arrow-alt-circle-down" data-toggle="popover" data-placement="right" data-content="No-Comments"></i></div>
									<div class="all_comments1" style="display:none">${comments}</div>
								</div>							
							</div>
							<p class="time">${elem.time}</p>
						</div>							
						<hr>
						`);
					$("#home").append(el);	
					$(".img_plus_post img[src='']").remove();
			});
		}
	});
	$(document).on("click",".show_coments1",function() {
		$(this).parents(".one").find(".all_comments1").slideToggle();
		$(this).after("<i class=\"comment_hide1 fas fa-arrow-alt-circle-up\"></i>").remove();

	});
	$(document).on("click", ".comment_hide1", function() {		
		$(this).parents(".one").find(".all_comments1").slideUp(400);
		$(this).after("<i class=\"show_coments1 fas fa-arrow-alt-circle-down\"></i>").remove();
		

	});
	// open user photos in their page
	$(".open_fr_img").click(function () {
		$(".img_wrap").remove();
		let friendId = $("#fr_id").val();
		console.log(friendId);
		$.ajax({
			url: "server.php",
			type: "POST",
			data: {action: "get_fr_photos", fr_id: friendId},
			success: function(r) {				
				let my_id = $("#my_id").val();
				r = JSON.parse(r);
				console.log(r);
				r.forEach(function(elem, index) {
					let comments = ``;
					elem.comments.forEach(function(item) {
					 	comments+=`
 								<div class="img_coment_res d-flex flex-column">
 									<ul class="img_com_inf d-flex">
 										<li><img src="${item.photo}" alt=""></li>
 										<li><a href="guest.php?id=${item.id}">${item.name} ${item.surname}</a></li>
 									</ul>
 									<div class="img_com_source">
 										<p class="img_p1">${item.comment}</p>
 										<p class="img_p2">${item.time}</p>
 									</div>
 								</div>`;
						
					});
				  let flag = elem.likes.some(e=>e.user_id == my_id);
					let el = $(`
				  	<div class="img_wrap position-relative">
				  	<div class="helper" style="display:none">${comments}</div>		  						  
						<img class="fnd" src="${elem.image}" alt="" data-img_ind="${elem.id}">
						<div class="icons position-absolute fixed-bottom d-flex justify-content-around">
${(flag) ? `<div class="dis_lk position-relative" data-img_id="${elem.id}"><i class="far fa-thumbs-down btn btn-danger btn-sx"></i><span class="position-absolute">${elem.likes.length}</span></div>` : `<div class="lk position-relative" data-img_id="${elem.id}"><i class="far fa-thumbs-up btn btn-success btn-sx"></i><span class="position-absolute">${elem.likes.length}</span></div>`}								 
							<div class="com position-relative"><i class="far fa-comments btn btn-success btn-sx"></i><span class="position-absolute">${elem.comments.length}</span></div> 
						</div>
					</div>					
					  `);
				$(".fr_img_show").append(el);
				});
				// $(".img_img_com").append(comment);
			}
		});
	});
	// open modal
	$(document).on("click", ".img_wrap", function(e) {
		$(".comm").empty();
		let src = $(this).find(".fnd").attr("src");
		let img_id = $(this).find(".fnd").attr("data-img_ind");
		let com = $(this).find(".helper").html();
		$(".comm").append(com);
		$(".view_img img").attr({"src": src, "data-img":img_id});
		$("#myModal").modal("show");

		console.log($(".comm").get(0).scrollHeight)

	});
	// like image
	$(document).on("click", ".lk", function(e) {
		e.stopPropagation();
		const my_id = $("#my_id").val();
		let imgId = $(this).attr("data-img_id");
		console.log(imgId);
		let am = +$(this).find("span").html() + 1;
		$(this).after(`<div class="dis_lk position-relative" data-img_id="${imgId}"><i class="far fa-thumbs-down btn btn-danger btn-sx"></i><span class="position-absolute">${am}</span></div>`).remove();
		$.ajax({
		url: "server.php",
		type: "POST",
		data: {action: "to_like_img", "img_id": imgId, troy:"true"},
		success: function(r) {
			console.log(r);
		}
	});

	});
	// dislike image
	$(document).on("click", ".dis_lk", function(e) {
		e.stopPropagation();
		let imgId = $(this).attr("data-img_id");
		let am = +$(this).find("span").html() - 1;
		$(this).after(`<div class="lk position-relative" data-img_id="${imgId}"><i class="far fa-thumbs-up btn btn-success btn-sx"></i><span class="position-absolute">${am}</span></div>`).remove();
		$.ajax({
		url: "server.php",
		type: "POST",
		data: {action: "to_like_img", "img_id": imgId, troy:"false"},
		success: function(r) {
			console.log(r);
			}
		});
	});
	// textarea trick
		// image comment butonn click
		$(document).on("click", ".com", function(e) {			
			// e.stopPropagation();

		});
	 $(".test").focusout(function(){
        var element = $(this);        
        if (!element.text().replace(" ", "").length) {
            element.empty();
        }
    });
	  $(".test").on('keypress', function(e){       
        if (e.key === "Enter") {
        	let user_id = $("#fr_id").val();
        	let value = $(this).text();
        	let img_id = $(this).prev().attr("data-img");
        	console.log(img_id);
            $(this).text("");
            if(value.trim().length){
            	$.ajax({
				url: "server.php",
				type: "POST",
				data: {action: "add_img_comm", "img_id": img_id, value: value, user_id: user_id},
				success: function(r) {
					//r = JSON.parse(r);
					console.log(r);
					}
				});
            }
            e.preventDefault();
        }
    });
	// $(document).on('click', '[data-toggle="lightbox"]', function(event) {
	 //    event.preventDefault();
	 //    $(this).ekkoLightbox({alwaysShowClose: true});
	//});
	// get friend friends
	$(".open_fr_fr").click(function() {
		let userId = $("#fr_id").val();
		$.ajax({
			url: "server.php",
			type: "POST",
			data: {action: "get_friend_friends", fr_id: userId},
			success: function(r) {
				$(".is_mutual:contains('Հարցումը')").css("fontSize", "10px");
				let my_id = $("#my_id").val();
				$(".overal_fr_fr").remove();
				r = JSON.parse(r);
				console.log(r);
				let friends = r[0];
				let friends_id = friends.map(it=>it.id);
				let mutualFriends = r[1].map(item=> item.id);
				let mtFrSendedRequest = r[2].map(item=> item.id);
				if(friends.length){
					let str = $(`<div class="overal_fr d-flex flex-wrap"></div>`);
					let str1 =$( `<div class="overal_fr_fr d-flex flex-column"></div>`);
					friends.forEach(function(elem, index) {
						let fr =" ";
						let flag = "remove";
						// let btnStyle = null;
						if(~mutualFriends.indexOf(elem.id)){
							fr = "Ընդհանուր Ընկեր"
						} else if (~mtFrSendedRequest.indexOf(elem.id)){
							flag = 'send';
						}else flag = 'add';
						var el = $(`							
								<div class="ul1 text-center border-top mr-2">								
									<a href="guest.php?id=${elem.id}">
${(flag == 'send') ? `<span class="is_mutual" style="font-size:10px">Հարցումը ուղարկված է</span>` : `<span class="is_mutual border-bottom-0">${fr}</span>`}
										<div class="comon d-flex flex-column border border-secondary rounded">
											<div class="for_pic">
												<img src="${elem.photo}">
											</div>
											<div class="for_name d-flex flex-column">
												<p class="border-bottom">${elem.name} ${elem.surname}</p>
${(flag == 'remove') ? `<button id=${elem.id} class="rounded del_frr bg-danger">Հեռացնել</button>`: (flag == 'send') ? `<button id=${elem.id} class="rounded add_frr_remove  bg-danger">Չեղարկել</button>` : `<button id=${elem.id} class="rounded add_frr">Ավելացնել</button>`}
											</div>										
										</div>
									</a>
							</div>
						`);
						el.appendTo(str);
					});
					$(str1).prepend(`<p><span>Ընկերներ ${friends_id.length},</span><a href="#" class="mt_fr_l">(${mutualFriends.length} ընդհանուր)</a> </p>`);
					str1.append(str);
					$('.fr_fr_list').prepend(str1);

				} else $(`<div class="overal_fr"></div>`).append(`<p>No any friend</p>`).appendTo('.fr_fr_list');
				if(~friends_id.indexOf(my_id)){
					$(`#${my_id}`).parents(".ul1").find(".is_mutual").html("Դուք");
					let id = $("#fr_id").val();
					$(`#${my_id}`).html("Դադարեցնել").removeClass('add_frr').addClass("del_frr bg-danger").attr("id", id);

				}
			}
		});
	});
	// go to mutual friend list
	$(document).on("click", ".mt_fr_l", function() {
		// $(".open_fr_img").click();
		alert(6);
	});
	// 
	$(document).on("click", ".add_frr", function(e) {
		let userId = $(this).attr("id");
		let self = $(this);
		$(this).html("Չեղարկել");
		$(this).addClass('add_frr_remove  bg-danger').removeClass('add_frr');
		
		$(".searching").empty();
		$.ajax({
			url : "server.php",
			type: "POST",
			data: {action: "add_request", user_id: userId},
			success: function(r){
				self.parents(".ul1").find(".is_mutual").css("fontSize", "10px").html("Հարցումը ուղարկված է");
			}
		});
		e.preventDefault();
	});
	$(document).on("click", ".add_frr_remove", function(e) {
		let userId = $(this).attr("id");
		$(this).html("Ավելացնել");
		$(this).addClass('add_frr').removeClass('add_frr_remove bg-danger');
		$(this).parents(".ul1").find(".is_mutual").html("");
		$(".search").val("");
		$(".searching").empty();
		$.ajax({
			url : "server.php",
			type: "POST",
			data: {action: "remove_request", user_id: userId},
			success: function(r){
			}
		});
		e.preventDefault();
	});
	// delete my friend(mutual) for friend friend fr lists
		$("body").on("click", ".del_frr", function(e) {
		let self = $(this);
		let id= $(this).attr("id");
		let fr_id = $("#fr_id").val();
		$.ajax({
			url : "server.php",
			type: "POST",
			data: {action: "del_friend", friend_id: id},
			success: function(r){
				if(self.attr("id") == fr_id){
					let sum = $(".overal_fr_fr p > span").html().match(/\d+/g)[0];
					console.log(sum);
					self.parents('.ul1').remove();
					$(".overal_fr_fr p > span").html(`Ընկերներ ${+sum - 1}`);
				} else {
					self.html("Ավելացնել").addClass('add_frr').removeClass('del_frr bg-danger');
					self.parents(".ul1").find(".is_mutual").html("");
				}
			}
		});
		e.preventDefault();
	});
});