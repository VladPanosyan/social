$(document).ready(function() {
	$("#plus_img").change(function(){
		// console.log($(this).val());
		$(".forma1").submit();		
	});
	// select all my pictures
	$.ajax({
		url: "server.php",
		type: "POST",
		data: {action:"sel_my_pics"},
		success: function(r) {
			r = JSON.parse(r);
			console.log(r);
			r.forEach( function(elem, index) {
				let el = $(`
					<div class="img_wrap">
						<img src="${elem.image}" alt="">
						<div class="btns" data-id="${elem.id}">
							<button class="set_img">set</button> 		
							<button class="del_img">delete</button>
						</div>
					</div>
					`);
				$(".wrapper").append(el);
			});
			
			console.log(r);
		}
	});
// change my profile phpto 
$(".wrapper").on("click", ".set_img", function() {
	let imgId = $(this).parent().attr("data-id");

	swal({
		title: "Are you sure?",
		text: "Once deleted, you will not be able to recover this imaginary file!",
		icon: "warning",
		buttons: true,
		dangerMode: true,
	})
	.then((willDelete) => {
		if (willDelete) {
			$.ajax({
				url: "server.php",
				type: "POST",
				data: {action:"set_my_pics", img_id: imgId},
				success: function(r) {
				// r = JSON.parse(r);
				console.log(r);
			}
		});
			swal("Poof! Your imaginary file has been deleted!", {
				icon: "success",
			});
		} else {
			swal("Your imaginary file is safe!");
		}
	});
	
});
	// delete an photo in my photos
	$(".wrapper").on("click", ".del_img", function() {
		let imgId = $(this).parent().attr("data-id");
		let t = $(this)
		swal({
			title: "Are you sure?",
			text: "Once deleted, you will not be able to recover this imaginary file!",
			icon: "warning",
			buttons: true,
			dangerMode: true,
		})
		.then((willDelete) => {
			if (willDelete) {

				t.parents(".img_wrap").remove()
				$.ajax({
					url: "server.php",
					type: "POST",
					data: {action:"del_my_pics", img_id: imgId},
					success: function(r) {
					}
				});
				swal("Poof! Your imaginary file has been deleted!", {
					icon: "success",
				});
			} else {
				swal("Your imaginary file is safe!");
			}
		});
		
	});
});