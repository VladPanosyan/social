/*
Navicat MySQL Data Transfer

Source Server         : Vlado
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : globbase

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-04-08 00:40:23
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `comment`
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `comment` varchar(250) DEFAULT NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `post_id` (`post_id`),
  CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `comment_ibfk_3` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of comment
-- ----------------------------
INSERT INTO `comment` VALUES ('47', '8', '6', 'ok', '2019-03-30 19:42:11');
INSERT INTO `comment` VALUES ('50', '8', '3', 'barev armenoid', '2019-03-30 20:13:03');
INSERT INTO `comment` VALUES ('51', '2', '3', 'du dud du', '2019-03-30 20:13:23');

-- ----------------------------
-- Table structure for `comment_img`
-- ----------------------------
DROP TABLE IF EXISTS `comment_img`;
CREATE TABLE `comment_img` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `image_id` int(11) DEFAULT NULL,
  `comment` varchar(256) DEFAULT NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `image_id` (`image_id`),
  CONSTRAINT `comment_img_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `comment_img_ibfk_2` FOREIGN KEY (`image_id`) REFERENCES `image` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of comment_img
-- ----------------------------
INSERT INTO `comment_img` VALUES ('1', '8', '27', '123', '2019-04-03 22:46:27');
INSERT INTO `comment_img` VALUES ('2', '8', '27', 'ol', '2019-04-03 17:22:20');
INSERT INTO `comment_img` VALUES ('3', '8', '28', 'dfdfdfdf', '2019-04-03 20:50:29');
INSERT INTO `comment_img` VALUES ('4', '8', '28', 'asasas', '2019-04-03 20:55:38');
INSERT INTO `comment_img` VALUES ('5', '8', '28', 'sds', '2019-04-03 20:56:21');
INSERT INTO `comment_img` VALUES ('6', '8', '28', 'sdsdsd', '2019-04-03 20:56:56');
INSERT INTO `comment_img` VALUES ('7', '4', '28', '777', '2019-04-03 22:46:43');
INSERT INTO `comment_img` VALUES ('8', '8', '28', 'sadsa', '2019-04-03 23:16:15');

-- ----------------------------
-- Table structure for `event`
-- ----------------------------
DROP TABLE IF EXISTS `event`;
CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `ev_name` varchar(250) DEFAULT NULL,
  `place` varchar(250) DEFAULT NULL,
  `disc` varchar(250) DEFAULT NULL,
  `time_start` timestamp NULL DEFAULT NULL,
  `time_end` timestamp NULL DEFAULT NULL,
  `fr_photo` varchar(256) DEFAULT NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `event_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of event
-- ----------------------------
INSERT INTO `event` VALUES ('18', '8', 'qqq', 'www', 'gfg', '2019-04-06 00:00:00', '2019-04-06 00:00:00', 'images_ev/1554497968download.jpg', '2019-04-07 21:03:49');
INSERT INTO `event` VALUES ('19', '8', 'erre', 'trt', 'ghg', '2020-04-06 00:00:00', '2019-04-06 00:00:00', 'https://encrypted-tbn0.gstatic.com/images?https://encrypted-tbn0.gstatic.com/images?images_ev/1554497968download.jpg', '2019-04-07 21:03:49');
INSERT INTO `event` VALUES ('20', '8', '77', 'ee', 'hjkmhj', '2019-04-06 03:00:00', '2020-04-06 00:00:00', 'images_ev/1554497968download.jpg', '2019-04-07 21:03:49');
INSERT INTO `event` VALUES ('21', '5', 'ghj', 'j', 'hjh', '2019-04-01 01:05:07', '2019-04-24 01:05:12', 'images_ev/1554497968download.jpg', '2019-04-07 21:03:49');
INSERT INTO `event` VALUES ('25', '8', 'hgh', 'gh', 'ghg', '2019-04-07 00:00:00', '2019-04-07 00:00:00', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTHx7PC2ckLdzqy2VvWXdVtljsEhAbsWwQIFvznFYxBaHE8MOqxXA', '2019-04-07 21:03:49');
INSERT INTO `event` VALUES ('26', '2', '2', '22', '222', null, null, 'images_ev/1554497968download.jpg', '2019-04-07 21:03:49');
INSERT INTO `event` VALUES ('27', '3', '3', '333', '333', null, null, 'images_ev/1554497968download.jpg', '2019-04-07 21:03:49');
INSERT INTO `event` VALUES ('28', '6', '666', '666', '666', null, null, 'images_ev/1554497968download.jpg', '2019-04-07 21:03:49');
INSERT INTO `event` VALUES ('29', '4', '444', '444', '444', null, null, 'images_ev/1554497968download.jpg', '2019-04-07 21:03:49');
INSERT INTO `event` VALUES ('30', '4', '414141', '14', '141', '0000-00-00 00:00:00', null, 'images_ev/1554497968download.jpg', '2019-04-07 21:03:49');

-- ----------------------------
-- Table structure for `ev_accept`
-- ----------------------------
DROP TABLE IF EXISTS `ev_accept`;
CREATE TABLE `ev_accept` (
  `user_id` int(11) NOT NULL,
  `ev_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`ev_id`),
  KEY `ev_id` (`ev_id`),
  CONSTRAINT `ev_accept_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ev_accept_ibfk_2` FOREIGN KEY (`ev_id`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ev_accept
-- ----------------------------
INSERT INTO `ev_accept` VALUES ('1', '18');
INSERT INTO `ev_accept` VALUES ('2', '18');
INSERT INTO `ev_accept` VALUES ('3', '18');
INSERT INTO `ev_accept` VALUES ('4', '19');
INSERT INTO `ev_accept` VALUES ('5', '20');
INSERT INTO `ev_accept` VALUES ('7', '29');

-- ----------------------------
-- Table structure for `ev_comment`
-- ----------------------------
DROP TABLE IF EXISTS `ev_comment`;
CREATE TABLE `ev_comment` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `comment` varchar(256) DEFAULT NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `event_id` (`event_id`),
  CONSTRAINT `ev_comment_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ev_comment_ibfk_2` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ev_comment
-- ----------------------------

-- ----------------------------
-- Table structure for `ev_interest`
-- ----------------------------
DROP TABLE IF EXISTS `ev_interest`;
CREATE TABLE `ev_interest` (
  `user_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`event_id`),
  KEY `event_id` (`event_id`),
  CONSTRAINT `ev_interest_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ev_interest_ibfk_2` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ev_interest
-- ----------------------------
INSERT INTO `ev_interest` VALUES ('6', '19');
INSERT INTO `ev_interest` VALUES ('6', '21');
INSERT INTO `ev_interest` VALUES ('7', '18');

-- ----------------------------
-- Table structure for `ev_like`
-- ----------------------------
DROP TABLE IF EXISTS `ev_like`;
CREATE TABLE `ev_like` (
  `user_id` int(11) NOT NULL,
  `ev_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`ev_id`),
  KEY `ev_id` (`ev_id`),
  CONSTRAINT `ev_like_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ev_like_ibfk_2` FOREIGN KEY (`ev_id`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ev_like
-- ----------------------------
INSERT INTO `ev_like` VALUES ('1', '18');
INSERT INTO `ev_like` VALUES ('2', '18');
INSERT INTO `ev_like` VALUES ('2', '19');
INSERT INTO `ev_like` VALUES ('2', '20');
INSERT INTO `ev_like` VALUES ('2', '29');
INSERT INTO `ev_like` VALUES ('3', '18');
INSERT INTO `ev_like` VALUES ('4', '19');
INSERT INTO `ev_like` VALUES ('5', '19');
INSERT INTO `ev_like` VALUES ('6', '18');
INSERT INTO `ev_like` VALUES ('6', '20');
INSERT INTO `ev_like` VALUES ('6', '29');
INSERT INTO `ev_like` VALUES ('8', '29');
INSERT INTO `ev_like` VALUES ('8', '30');

-- ----------------------------
-- Table structure for `friend`
-- ----------------------------
DROP TABLE IF EXISTS `friend`;
CREATE TABLE `friend` (
  `my_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`my_id`,`user_id`),
  KEY `user_id` (`my_id`) USING BTREE,
  KEY `user_id_2` (`user_id`),
  CONSTRAINT `friend_ibfk_1` FOREIGN KEY (`my_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `friend_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of friend
-- ----------------------------
INSERT INTO `friend` VALUES ('4', '8');
INSERT INTO `friend` VALUES ('5', '1');
INSERT INTO `friend` VALUES ('5', '4');
INSERT INTO `friend` VALUES ('5', '7');
INSERT INTO `friend` VALUES ('6', '8');
INSERT INTO `friend` VALUES ('7', '6');
INSERT INTO `friend` VALUES ('8', '1');

-- ----------------------------
-- Table structure for `image`
-- ----------------------------
DROP TABLE IF EXISTS `image`;
CREATE TABLE `image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `my_id` int(11) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `my_id` (`my_id`),
  CONSTRAINT `image_ibfk_1` FOREIGN KEY (`my_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of image
-- ----------------------------
INSERT INTO `image` VALUES ('26', '8', 'my_images/1553704395images (4).jpg');
INSERT INTO `image` VALUES ('27', '5', 'my_images/1553975086download.jpg');
INSERT INTO `image` VALUES ('28', '5', 'my_images/1553975090images (1).jpg');
INSERT INTO `image` VALUES ('29', '5', 'my_images/1553975093images.jpg');
INSERT INTO `image` VALUES ('30', '8', 'my_images/1554227985download.jpg');

-- ----------------------------
-- Table structure for `like`
-- ----------------------------
DROP TABLE IF EXISTS `like`;
CREATE TABLE `like` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `post_id` (`post_id`),
  CONSTRAINT `like_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `like_ibfk_3` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of like
-- ----------------------------
INSERT INTO `like` VALUES ('42', '8', '3');
INSERT INTO `like` VALUES ('43', '8', '4');

-- ----------------------------
-- Table structure for `like_img`
-- ----------------------------
DROP TABLE IF EXISTS `like_img`;
CREATE TABLE `like_img` (
  `user_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`image_id`),
  KEY `image_id` (`image_id`),
  CONSTRAINT `like_img_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `like_img_ibfk_2` FOREIGN KEY (`image_id`) REFERENCES `image` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of like_img
-- ----------------------------
INSERT INTO `like_img` VALUES ('1', '27');
INSERT INTO `like_img` VALUES ('2', '27');
INSERT INTO `like_img` VALUES ('3', '27');
INSERT INTO `like_img` VALUES ('5', '28');
INSERT INTO `like_img` VALUES ('8', '27');
INSERT INTO `like_img` VALUES ('8', '29');

-- ----------------------------
-- Table structure for `message`
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `my_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `message` varchar(256) DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  `open` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `my_id` (`my_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `message_ibfk_1` FOREIGN KEY (`my_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `message_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of message
-- ----------------------------
INSERT INTO `message` VALUES ('59', '2', '8', '2-8-', null, '1', '1');
INSERT INTO `message` VALUES ('60', '5', '8', '5-8', null, '1', '1');
INSERT INTO `message` VALUES ('61', '6', '8', '6-8', null, '1', '1');
INSERT INTO `message` VALUES ('62', '2', '8', '2-8', null, '1', '1');
INSERT INTO `message` VALUES ('63', '2', '8', '2-8', null, '1', '1');
INSERT INTO `message` VALUES ('64', '6', '8', '6-8', null, '1', '1');
INSERT INTO `message` VALUES ('65', '6', '8', '6-8', null, '1', '1');
INSERT INTO `message` VALUES ('66', '7', '8', '7-8', null, '0', '1');
INSERT INTO `message` VALUES ('67', '7', '8', '7-8', null, '0', '1');
INSERT INTO `message` VALUES ('68', '7', '8', '7-8', null, '0', '1');
INSERT INTO `message` VALUES ('69', '7', '8', '7-8', null, '0', '1');
INSERT INTO `message` VALUES ('83', '4', '8', '4-8', null, '1', '1');
INSERT INTO `message` VALUES ('86', '3', '8', 'erererer', '1553954918', '1', '1');
INSERT INTO `message` VALUES ('87', '3', '8', 'yyyyyyy', '1553954943', '1', '1');
INSERT INTO `message` VALUES ('88', '3', '8', '44444444', '1553955209', '1', '1');
INSERT INTO `message` VALUES ('89', '8', '6', 'fgfgf', '1554214729', '0', '0');
INSERT INTO `message` VALUES ('90', '8', '6', 'fgfgfgfgf', '1554214775', '0', '0');
INSERT INTO `message` VALUES ('91', '8', '6', 'ttt', '1554214888', '0', '0');
INSERT INTO `message` VALUES ('92', '8', '6', 'ttt', '1554214900', '0', '0');
INSERT INTO `message` VALUES ('93', '8', '6', 'fgfg', '1554215010', '0', '0');
INSERT INTO `message` VALUES ('94', '8', '6', 'fdfdf', '1554215112', '0', '0');
INSERT INTO `message` VALUES ('95', '8', '6', 'rgtr', '1554215175', '0', '0');
INSERT INTO `message` VALUES ('96', '8', '6', 'yyyyy', '1554215180', '0', '0');
INSERT INTO `message` VALUES ('97', '8', '6', 'yyyyy', '1554215195', '0', '0');
INSERT INTO `message` VALUES ('98', '8', '6', 'seufio iewur uf iuiuf iuf u8uf w8i w8uf 9ud9uw 9udfwjf uw8qu   wyud8 wqd8uydf ', '1554216190', '0', '0');
INSERT INTO `message` VALUES ('99', '6', '8', '222222', null, '1', '1');
INSERT INTO `message` VALUES ('100', '8', '6', '6', null, '0', '0');
INSERT INTO `message` VALUES ('101', '8', '6', 'dcd', '1554225827', '0', '0');
INSERT INTO `message` VALUES ('102', '8', '6', 'tttt', '1554225841', '0', '0');

-- ----------------------------
-- Table structure for `post`
-- ----------------------------
DROP TABLE IF EXISTS `post`;
CREATE TABLE `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `my_id` int(11) DEFAULT NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `post` varchar(250) DEFAULT NULL,
  `post_img` varchar(200) DEFAULT NULL,
  `post_video` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `my_id` (`my_id`),
  CONSTRAINT `post_ibfk_1` FOREIGN KEY (`my_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of post
-- ----------------------------
INSERT INTO `post` VALUES ('3', '5', '2019-03-30 17:56:29', 'Armen', null, null);
INSERT INTO `post` VALUES ('4', '8', '2019-03-30 17:56:44', '4', null, null);
INSERT INTO `post` VALUES ('6', '7', '2019-03-30 17:56:56', '6', null, null);
INSERT INTO `post` VALUES ('19', '8', '2019-04-01 22:23:22', '', '', 'images_post_vidst/1554143002post_max_size.mp4');
INSERT INTO `post` VALUES ('21', '8', '2019-04-01 22:55:55', '', 'images_post/1554144955images (1).jpg', '');

-- ----------------------------
-- Table structure for `request`
-- ----------------------------
DROP TABLE IF EXISTS `request`;
CREATE TABLE `request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `my_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `request_ibfk_1` (`my_id`),
  CONSTRAINT `request_ibfk_1` FOREIGN KEY (`my_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of request
-- ----------------------------
INSERT INTO `request` VALUES ('1', '8', '1');
INSERT INTO `request` VALUES ('2', '8', '7');
INSERT INTO `request` VALUES ('4', '3', '8');
INSERT INTO `request` VALUES ('5', '2', '8');
INSERT INTO `request` VALUES ('8', '8', '5');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `surname` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `age` int(3) DEFAULT NULL,
  `email` varchar(320) CHARACTER SET latin1 DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `photo` varchar(200) COLLATE latin1_general_ci DEFAULT NULL,
  `online` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'Karen', 'Karapetyan', '99', 'qqqqqq@eee.ii', '$2y$10$Hqt7HNNhFhah2BAGtb0w2.j2f8nTJnHpZfIjztGcvlaqRmXvcRgmS', 'images/1552914434images (1).jpg', '0');
INSERT INTO `user` VALUES ('2', 'Ashot', 'Sargsyan', '88', 'scfw@sd.oo', '$2y$10$j4jfMHHl2rxLruYquqfy4ucZbqVq1.e8DbrE64K/IlIuJks7ydZPe', 'images/1553630208images (2).jpg', '0');
INSERT INTO `user` VALUES ('3', 'Arsen', 'Tigranyan', '77', 'asas@adsa.uu', '$2y$10$CpGNPo22gyfcMdss.sCKhOwp9I1yNNfUWnl4GaPeDRjSUDKADnVe2', 'images/1552914599images (3).jpg', '0');
INSERT INTO `user` VALUES ('4', 'Vazgen', 'Asatryan', '33', 'adsafd@wsfs.ll', '$2y$10$TMjdtz0a1hzf.pF9qARKnurSuw2u7A8V.ODDd74LzuTHM4Wc6CT8a', 'images/1552914621images (4).jpg', '0');
INSERT INTO `user` VALUES ('5', 'Armen', 'Badalyan', '66', 'asdad@asas.pp', '$2y$10$77rcUZYyD4UJHarIFXHUreCYPHglF9qStWdFT9rCWnadtuXKhW/dW', 'images/1552914645images.jpg', '0');
INSERT INTO `user` VALUES ('6', 'Artak', 'Manasyan', '55', 'asxasdc@sds.oo', '$2y$10$7rTHn8ph8C17KyOCF0cBaeq7XCRn.hRx2WWogMaOGnZutCEdH3xLK', 'images/1552914673redpanda-profile-400x400-984.jpg', '0');
INSERT INTO `user` VALUES ('7', 'Aghas', 'Davtyan', '44', 'wdscvf@sdc.tt', '$2y$10$zkfTIGeL07rWRmc7LY8gfejMOLdlMTblL7d79i6487C6ZBkr52CcS', 'images/1552914756images (5).jpg', '0');
INSERT INTO `user` VALUES ('8', 'Volodya', 'Panosyan', '33', 'askjxoknjwx@wdsd.yy', '$2y$10$cfmaC8WAupIa5WMslFRrK.LNgcHBMSrbeR0FS2Xh/AYDDw6pZrLDS', 'images/1553787488images (5).jpg', '1');
